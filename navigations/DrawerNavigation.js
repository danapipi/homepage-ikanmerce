import React, { Component } from "react";
import { NavigationActions, DrawerItems } from "react-navigation";
import {
  Text,
  View,
  StyleSheet,
  ImageBackground,
  ScrollView,AsyncStorage,TouchableOpacity
} from "react-native";
import { SearchBar, Header, Icon, Avatar } from "react-native-elements";

const customDrawer = props => {
  return (
    <View>
      <ScrollView>
        <View style={styles.container}>
          <View style={styles.headerContainer}>
            <ImageBackground
              source={require("../assets/bg-drawer.jpg")}
              style={{
                flex: 1,
                width: 235,
                justifyContent: "center",
                alignItems: "flex-start"
              }}
            >
              <Avatar
                medium
                rounded
                source={require("../assets/avatar-tes.jpg")}
                avatarStyle={styles.avatar}
              />
            </ImageBackground>
          </View>
        </View>
        <DrawerItems
          {...props}
          getLabel={scene => {
            if(scene.route.key!=="Login"&&scene.route.key!=="Register"&&scene.route.key!=="Checkout"&&scene.route.key!=="Intro")
            return (
              <View
                style={{
                  flex: 1,
                  justifyContent: "center",
                  paddingVertical: 15,
                  marginLeft: 40
                }}
              >
                <Text style={styles.screenTextStyle}>
                  {props.getLabel(scene)}
                </Text>
              </View>
            );
            else return null
          }}
        />
      </ScrollView>
      <TouchableOpacity
            style={{
              margin: 10,
              padding: 10,
              backgroundColor: "#45aaf2",
              elevation: 4,
              borderRadius: 20
            }}
            onPress={()=>{
              AsyncStorage.clear()
              props.navigation.goBack();
            }}
          >
            <Text
              style={{
                color: "white",
                textAlign: "center",
                fontWeight: "bold",
                fontSize: 18
              }}
            >
              Log Out
            </Text>
          </TouchableOpacity>
    </View>
  );
};

export default customDrawer;

const styles = StyleSheet.create({
  container: {
    alignItems: "center"
  },
  headerContainer: {
    height: 150
  },
  headerText: {
    color: "#fff8f8"
  },
  screenContainer: {
    paddingTop: 20
  },
  screenStyle: {
    height: 30,
    marginTop: 2,
    flexDirection: "row",
    alignItems: "center"
  },
  screenTextStyle: {
    fontSize: 18,
    color: "black"
  },
  avatar: {
    alignContent: "center",
    justifyContent: "center",
    marginVertical: 30
  }
});
