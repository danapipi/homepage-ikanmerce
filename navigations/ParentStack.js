import React, { Component } from "react";
import {
  View,
  ScrollView,
  Text,
  ImageBackground,
  StyleSheet
} from "react-native";
import {
  createStackNavigator,
  createDrawerNavigator,
  DrawerItems
} from "react-navigation";
import UserHome from "../pages/User/UserHome";
import UCustomDrawer from "../navigations/DrawerNavigation";
import UCart from "../pages/User/Cart";
import UFishList from "../pages/User/FishList";
import CartList from "../pages/User/CartList";
import Checkout from "../pages/User/Checkout";
import Details from "../pages/User/Details";
import Login from "../pages/LoginScreen";
import Regis from "../pages/RegisterScreen";
import Instructions from "../pages/Instructions";
import App from "../pages/admin/App";
import customDrawer from "../pages/admin/drawerContentComponents";
import Cart from "../pages/admin/Cart";
import FishList from "../pages/admin/FishList";
import Chat from "../pages/admin/Chat";
import addFish from "../pages/admin/addFish";
import addCategory from "../pages/admin/addCategory";
import editCategory from "../pages/admin/editCategory";
import editItem from "../pages/admin/editItem";
import cartDetails from "../pages/admin/cartDetails";
import viewCategory from "../pages/admin/viewCategory";

export const stack = createStackNavigator({
  FishList: {
    screen: FishList
  },
  addFish: {
    screen: addFish
  },
  addCategory: {
    screen: addCategory
  },
  editCategory: {
    screen: editCategory
  },
  editItem: {
    screen: editItem
  },
  cartDetails: {
    screen: cartDetails
  },
  viewCategory: {
    screen: viewCategory
  }
});

export const stack2 = createStackNavigator({
  Cart: {
    screen: Cart
  },
  cartDetails: {
    screen: cartDetails
  }
});

export const fishStack = createStackNavigator({
  FishList: { path: "/", screen: UFishList },
  Details: { path: "/", screen: Details }
});

export const userDrawer = createDrawerNavigator(
  {
    Home: {
      path: "/",
      screen: UserHome
    },

    "Fish List": {
      path: "/",
      screen: fishStack
    },

    Cart: {
      path: "/sent",
      screen: UCart
    },
    "Cart History": {
      path: "/sent",
      screen: CartList
    },
    Checkout: {
      path: "/sent",
      screen: Checkout
    }
  },
  {
    initialRouteName: "Home",
    drawerPosition: "left",
    drawerWidth: 235,
    contentComponent: UCustomDrawer
  }
);

export const adminDrawer = createDrawerNavigator(
  {
    Home: {
      screen: App
    },
    FishList: {
      screen: stack
    },

    Cart: {
      screen: stack2
    },
    Chat: {
      screen: Chat
    }
  },
  {
    initialRouteName: "Home",
    drawerPosition: "left",
    drawerWidth: 235,
    contentComponent: customDrawer
  }
);

export default (logStack = createStackNavigator(
  {
    Intro: {
      path: "/",
      screen: Instructions
    },
    Login: {
      path: "/",
      screen: Login
    },
    Register: {
      path: "/",
      screen: Regis
    },
    User: {
      path: "/",
      screen: userDrawer
    },
    Admin: {
      path: "/",
      screen: adminDrawer
    }
  },
  {
    initialRouteName: "Intro",
    navigationOptions: {
      header: null
    }
  }
));

const styles = StyleSheet.create({
  container: {
    alignItems: "center"
  },
  headerContainer: {
    height: 150
  },
  headerText: {
    color: "#fff8f8"
  },
  screenContainer: {
    paddingTop: 20
  },
  screenStyle: {
    height: 30,
    marginTop: 2,
    flexDirection: "row",
    alignItems: "center"
  },
  screenTextStyle: {
    fontSize: 20,
    marginLeft: 20
  }
});
