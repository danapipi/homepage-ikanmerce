import React, { Component } from "react";
import {
  View,
  Text,
  Image,
  ActivityIndicator,
  FlatList,
  AsyncStorage,
  TouchableOpacity,
  ImageBackground,
  Button,
  Dimensions
} from "react-native";
import { SearchBar, Header, Icon } from "react-native-elements";
import CommonHeader from "../../assets/CommonHeader";

export default class Cart extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataSource: [],
      passJ: []
    };
  }

  static navigationOptions = {
    header: null,
    drawerLabel: "Cart"
  };

  searchChange() {
    return "sukses";
  }
  clearSearch() {
    return "hilang";
  }

  async getIdItem(itemId) {
    let link = `https://ikapi.herokuapp.com/api/invoice/${itemId}`;
    console.warn(link);
    return fetch(link, {
      method: "GET",
      headers: new Headers({
        "Content-Type": "application/json",
        Authorization: `Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImFkbWluQGdtYWlsLmNvbSIsImNyZWF0ZWRBdCI6IjIwMTgtMDgtMDJUMTM6MjQ6NTUuOTUzWiIsImlhdCI6MTUzMzIxNjI5NX0.W3h4jQr85Z_yA6W4xjvt_7TkbIwwL_klOL1voIOGPeE`
      })
    });
  }

  async touch(id) {
    const touchId = await this.getIdItem(id);

    const jsonP = JSON.parse(touchId._bodyInit).data;
    this.setState({
      passJ: jsonP
    });
    return this.props.navigation.navigate("cartDetails", { data: jsonP });
  }

  //   List Category
  renderItem = ({ item }) => {
    console.warn(item);
    return (
      <TouchableOpacity
        style={{
          borderRadius: 10,
          marginHorizontal: 15,
          marginBottom: 10,
          overflow: "hidden",
          shadowColor: "rgba(0, 0, 0, 0.75)",
          shadowOffset: { width: -1, height: 1 },
          shadowRadius: 10,
          elevation: 3,
          width: Dimensions.get("window").width - 30
        }}
        onPress={() => this.touch(item.id)}
      >
        <View style={{ padding: 20, marginVertical: 5 }}>
          <Text style={{ color: "black" }}>Invoice No : {item.id}</Text>
          <Text>User : {item.user.name}</Text>
          <Text>
            Total Price : Rp{" "}
            {item.total_price
              .toFixed(2)
              .replace(".", ",")
              .replace(/\d(?=(\d{3})+\,)/g, "$&.")}
          </Text>
          <Text>
            Status Payment :
            {item.status ? (
              <Text style={{ color: "red" }}> Pending</Text>
            ) : (
              <Text style={{ color: "green" }}> Success</Text>
            )}
          </Text>
        </View>
      </TouchableOpacity>
    );
    // console.warn(item.category_image);
  };

  async componentDidMount() {
    const url = `https://ikapi.herokuapp.com/api/invoice`;
    // console.warn("COMPONENT DID MOUNT:", url);

    AsyncStorage.getItem("token", (error, tokenLog) => {
      // console.warn("TEST");
      this.setState({ loading: false });

      fetch(url, {
        method: "GET",
        headers: new Headers({
          "Content-Type": "application/json",
          Authorization: `Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImFkbWluQGdtYWlsLmNvbSIsImNyZWF0ZWRBdCI6IjIwMTgtMDgtMDJUMTM6MjQ6NTUuOTUzWiIsImlhdCI6MTUzMzIxNjI5NX0.W3h4jQr85Z_yA6W4xjvt_7TkbIwwL_klOL1voIOGPeE`
        })
      })
        .then(response => {
          console.warn("RES IS RUNNING");
          console.log("response:", response);
          response.json().then(responseJson => {
            console.warn("RUNNING", responseJson.data);
            this.setState({
              dataSource: responseJson.data
            });
          });
        })
        .catch(error => {
          console.warn("COMPONENT MOUNT ERR : ", error);
        });
    });
  }

  renderFooter() {
    return this.state.loading ? (
      <ActivityIndicator
        animating={this.state.loading}
        color="#bc2b78"
        size="large"
      />
    ) : null;
  }

  renderSeparator = () => {
    return <View />;
  };

  render() {
    const { navigate } = this.props.navigation;

    return (
      <View>
        <CommonHeader admin parent={this} />
        <View
          style={{
            justifyContent: "center",
            alignItems: "center"
          }}
        >
          <Text
            style={{
              color: "white",
              fontSize: 20,
              textShadowColor: "rgba(0, 0, 0, 0.75)",
              textShadowOffset: { width: -1, height: 1 },
              textShadowRadius: 10,
              marginVertical: 10
            }}
          >
            Transaction History
          </Text>
        </View>
        <View>
          {this.state.dataSource.length > 0 ? (
            <View style={{ height: Dimensions.get("window").height - 150 }}>
              <FlatList
                data={this.state.dataSource}
                renderItem={this.renderItem}
                extraData={this.state}
                keyExtractor={(item, index) => index.toString()}
                ItemSeparatorComponent={this.renderSeparator}
                renderFooter={this.renderFooter.bind(this)}
              />
            </View>
          ) : (
            <ActivityIndicator size="large" color="#45aaf2" />
          )}
        </View>
      </View>
    );
  }
}
