import React, { Component } from "react";
import {
  View,
  Text,
  ScrollView,
  FlatList,
  AsyncStorage,
  TouchableOpacity,
  Image,
  Dimensions,
  ActivityIndicator,
  ImageBackground
} from "react-native";
import {
  SearchBar,
  Header,
  Icon,
  FormInput,
  FormLabel,
  Button
} from "react-native-elements";
import ImagePicker from "react-native-image-crop-picker";
import CommonHeader from "../../assets/CommonHeader"

export default class addCategory extends Component {
  constructor(props) {
    super(props);
    this.state = {
      category_name: "",
      category_image: null,
      dataSource: [],
      image: null,
      images: null,
      loading: false,
      passJ: []
    };
  }

  static navigationOptions = {
    header: null
  };

  categoryInput = (text, field) => {
    if ((field = "categoryName")) {
      this.setState({
        category_name: text
      });
    }
  };

  // refresh f
  refresh() {
    this.forceUpdate();
  }

  // Gallery function
  pickSingle = async (cropit, circular = false) => {
    const image = await ImagePicker.openPicker({
      width: 300,
      height: 300,
      cropping: cropit,
      cropperCircleOverlay: circular,
      compressImageMaxWidth: 640,
      compressImageMaxHeight: 480,
      compressImageQuality: 0.5,
      includeExif: true
    });
    this.setState({
      category_image: {
        uri: image.path,
        type: image.mime,
        name: "apaaja.jpg"
      }
    });

    // console.warn("eroer", this.state.category_image);
  };

  renderImage() {
    return (
      <Image
        style={{
          width: Dimensions.get("window").width / 2 - 5,
          height: Dimensions.get("window").width / 2 - 5,
          resizeMode: "contain",
          margin: 2.5
        }}
        source={this.state.category_image}
      />
    );
  }

  async getId(id) {
    let link = `https://ikapi.herokuapp.com/api/categories/${id}`;
    console.warn(link);
    return fetch(link, {
      method: "GET",
      headers: new Headers({
        "Content-Type": "application/json",
        Authorization: `Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImFkbWluQGdtYWlsLmNvbSIsImNyZWF0ZWRBdCI6IjIwMTgtMDgtMDJUMTM6MjQ6NTUuOTUzWiIsImlhdCI6MTUzMzIxNjI5NX0.W3h4jQr85Z_yA6W4xjvt_7TkbIwwL_klOL1voIOGPeE`
      })
    });
  }

  // submit
  submitCategory = async () => {
    let url = `https://ikapi.herokuapp.com/api/categories`;
    try {
      const token = await AsyncStorage.getItem("token");

      try {
        const formData = new FormData();
        formData.append("category_name", this.state.category_name);
        formData.append("category_image", this.state.category_image);

        fetch(url, {
          method: "POST",
          body: formData,
          headers: new Headers({
            "Content-Type": "multipart/form-data",
            Authorization: `Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImFkbWluQGdtYWlsLmNvbSIsImNyZWF0ZWRBdCI6IjIwMTgtMDgtMDJUMTM6MjQ6NTUuOTUzWiIsImlhdCI6MTUzMzIxNjI5NX0.W3h4jQr85Z_yA6W4xjvt_7TkbIwwL_klOL1voIOGPeE`
          })
        }).then(() => {
          this.props.navigation.navigate("FishList");
          // console.warn("sukses:", token);
        });
      } catch (error) {
        console.warn("Error1:", error);
      }
    } catch (error) {
      console.warn("Error2:", error);
    }
  };

  render() {
    return (
      <ScrollView style={{ flex: 1 }}>
        <CommonHeader admin parent={this}/>
        <FormLabel labelStyle={{ fontSize: 15, color: "#2d3436" }}>
          Fish Category
        </FormLabel>
        <FormInput
          inputStyle={{
            borderBottomColor: "#2d3436",
            borderBottomWidth: 1,
            color: "#2d3436"
          }}
          onChangeText={text => this.categoryInput(text, "categoryName")}
        />
        <FormLabel labelStyle={{ fontSize: 15, color: "#2d3436" }}>
          Background Image
        </FormLabel>
        <FormLabel labelStyle={{ fontSize: 10, color: "grey", marginTop: 5 }}>
          max 1 image
        </FormLabel>
        <View>
          <Button
            icon={{
              type: "entypo",
              name: "images",
              size: 15,
              color: "#45aaf2"
            }}
            onPress={() => this.pickSingle(false)}
            buttonStyle={{
              backgroundColor: "transparent",
              width: 70,
              height: 45,
              borderColor: "#45aaf2",
              borderWidth: 1,
              borderRadius: 5,
              marginVertical: 20
            }}
          />
          {this.state.category_image ? this.renderImage() : <Text />}
          <View />
          <View style={{ justifyContent: "center", alignItems: "center" }}>
            <Button
              title="SUBMIT"
              onPress={() => this.submitCategory()}
              buttonStyle={{
                backgroundColor: "#45aaf2",
                width: 150,
                height: 45,
                borderColor: "transparent",
                borderWidth: 1,
                borderRadius: 20,
                marginVertical: 20
              }}
            />
          </View>
        </View>
      </ScrollView>
    );
  }
}
