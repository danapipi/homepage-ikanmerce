import React, { Component } from "react";
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  TextInput,
  ScrollView,
  StyleSheet,
  PixelRatio,
  Dimensions,
  AsyncStorage,
  Alert
} from "react-native";
import {
  SearchBar,
  Header,
  Icon,
  FormInput,
  FormLabel,
  Button
} from "react-native-elements";
import ImagePicker from "react-native-image-crop-picker";
import CommonHeader from "../../assets/CommonHeader";

export default class editItem extends Component {
  constructor(props) {
    super(props);
    this.params = this.props.navigation.state.params;
    this.state = {
      item_name: "",
      description: "",
      specification: "",
      item_details: [],
      tags: [],
      category: "",
      pictures: [],
      resultList: "",
      image: null,
      images: null,
      length: 0,
      category_id: "",
      featured_image: null,
      featured_image_name: "",
      image_name: [],
      images_id: []
    };
  }

  static navigationOptions = {
    header: null,
    drawerLabel: "Add List"
  };

  // category_id
  categoryID() {
    // console.warn(this.params.data[0][0]);
    this.setState({
      category_id: this.params.data[0].category_id
    });
  }

  componentDidMount() {
    this.categoryID();
    this.state.item_details = this.params.data[0].item_details;
    this.state.length = this.params.data[0].item_details.length;
  
  }

  // input product
  updateItem(text, field) {
    if ((field = "itemName")) {
      this.setState({
        item_name: text
      });
    }
  }

  //  input description
  updateDesc(text, field) {
    if ((field = "desc")) {
      this.setState({
        description: text
      });
    }
  }

  //  input spesification
  updateSpec(text, field) {
    if ((field = "spec")) {
      this.setState({
        specification: text
      });
    }
  }
  // input thumbnail image name
  bgName(text, field) {
    if ((field = "bgName")) {
      this.setState({
        featured_image_name: text
      });
    }
  }

  //  input variation
  async variationItem() {
    
    this.state.length = await  this.state.length + 1;
    await this.state.item_details.push({
      id: this.state.length,
      item_id : this.params.data[0].item_details[0].item_id,
      size: "",
      price: 0,
      discount: "",
      stock: 0
    });
    this.forceUpdate()
  }

  munculVariasi() {
    // console.warn("DEBUG",this.state.item_details)
    return this.state.item_details.map((item, index) => {
      // console.warn("WEEEEE",index);
      return (
        <View
          style={{
            borderColor: "#2d3436",
            borderRadius: 5,
            borderWidth: 1,
            margin: 15
          }}
        >
          <FormLabel>Item details ID</FormLabel>
          <FormInput
            inputStyle={{ borderBottomColor: "#2d3436" }}
            value={String(this.params.data[0].item_details[index].id)}
            onSubmitEditing={() => console.warn("item details id:",this.state.item_details[index] )}
          />
          <FormLabel>Item ID</FormLabel>
          <FormInput
            inputStyle={{ borderBottomColor: "#2d3436" }}
            value={String(this.params.data[0].item_details[index].item_id)}
            onSubmitEditing={() => console.warn("item id:",this.state.item_details[index])}
          />
          <FormLabel>Old Size</FormLabel>
          <FormInput
            inputStyle={{ borderBottomColor: "#2d3436" }}
            value={String(this.params.data[0].item_details[index].size)}
            onSubmitEditing={() => console.warn(this.state.item_details[index])}
          />
          <FormLabel>New Size</FormLabel>
          <FormInput
            inputStyle={{ borderBottomColor: "#2d3436" }}
            onChangeText={text => (this.state.item_details[index].size = text)}
            onSubmitEditing={() => console.warn(this.state.item_details[index])}
          />
          <FormLabel>Old Price</FormLabel>
          <FormInput
            inputStyle={{ borderBottomColor: "#2d3436" }}
            value={String(this.params.data[0].item_details[index].price)}
            onSubmitEditing={() => console.warn(this.state.item_details[index])}
          />
          <FormLabel>New Price</FormLabel>
          <FormInput
            onChangeText={text => (this.state.item_details[index].price = text)}
            onSubmitEditing={() => console.warn(this.state.item_details[index])}
          />
          <FormLabel>Old Discount</FormLabel>
          <FormInput
            inputStyle={{ borderBottomColor: "#2d3436" }}
            value={String(this.params.data[0].item_details[index].discount)}
            onSubmitEditing={() => console.warn(this.state.item_details[index])}
          />
          <FormLabel>New Discount</FormLabel>
          <FormInput
            onChangeText={text =>
              (this.state.item_details[index].discount = text)
            }
            onSubmitEditing={() => console.warn(this.state.item_details[index])}
          />
          <FormLabel>Old Stock</FormLabel>
          <FormInput
            inputStyle={{ borderBottomColor: "#2d3436" }}
            value={String(this.params.data[0].item_details[index].stock)}
            onSubmitEditing={() => console.warn(this.state.item_details[index])}
          />
          <FormLabel>New Stock</FormLabel>
          <FormInput
            onChangeText={text => (this.state.item_details[index].stock = text)}
            onSubmitEditing={() => console.warn(this.state.item_details[index])}
          />
        </View>
      );
    });
  }

  // Tags
  inputTag(text) {
    this.state.tags.push(text);
    console.log("awas", this.state.tags);
    this.forceUpdate();
  }

  viewTag() {
    const arrView = [...this.state.tags];
    return arrView.map(item => {
      return (
        <View
          style={{
            borderColor: "transparent",
            borderRadius: 20,
            borderWidth: 1,
            height: 20,
            paddingHorizontal: 10,
            backgroundColor: "#ecf0f1",
            marginVertical: 10,
            elevation: 4,
            marginLeft: 15
          }}
        >
          <Text style={{ color: "#2d3436" }}>{item}</Text>
        </View>
      );
    });
  }

  // Search
  searchChange() {
    return "sukses";
  }
  clearSearch() {
    return "hilang";
  }

  // Camera function

  async pickSingleWithCamera(cropping) {
    const getCamera = await ImagePicker.openCamera({
      cropping: cropping,
      width: 200,
      height: 200,
      // includeExif: true,
      includeBase64: true
    });

    const imageItem = {
      uri: getCamera.path,
      type: getCamera.mime,
      name: "ikanlagi.jpg"
    };
    console.warn("cam:", imageItem);
    this.state.pictures.unshift(imageItem);

    if (this.state.pictures.length > 4) {
      this.state.pictures.pop();
    }
    this.forceUpdate();
  }

  // Gallery single
  pickSingle = async (cropit, circular = false) => {
    const image = await ImagePicker.openPicker({
      width: 300,
      height: 300,
      cropping: cropit,
      cropperCircleOverlay: circular,
      compressImageMaxWidth: 640,
      compressImageMaxHeight: 480,
      compressImageQuality: 0.5,
      includeExif: true
    });
    this.setState({
      featured_image: {
        uri: image.path,
        type: image.mime,
        name: "apaaja.jpg"
      }
    });

    // console.warn("eroer", this.state.category_image);
  };

  renderThumbImage() {
    return (
      <Image
        style={{
          width: Dimensions.get("window").width / 2 - 5,
          height: Dimensions.get("window").width / 2 - 5,
          resizeMode: "contain",
          marginVertical: 4.5,
          marginLeft: 15
        }}
        source={this.state.featured_image}
      />
    );
  }

  // Gallery function
  async pickMultiple() {
    const galleryItem = await ImagePicker.openPicker({
      multiple: true,
      waitAnimationEnd: false,
      // includeExif: true,
      includeBase64: true,
      compressImageMaxWidth: 100,
      compressImageMaxHeight: 100,
      // compressImageQuality: 0.8,
      enableRotationGesture: true
    });
    console.warn("ok", galleryItem[0].size);
    const imageItem = galleryItem.map(item => {
      return {
        uri: item.path,
        type: item.mime,
        name: "ikangaler.jpg"
      };
    });
    console.log("image item: ", imageItem);
    this.state.pictures = [...imageItem, ...this.state.pictures];
    if (this.state.pictures.length > 4) {
      this.state.pictures.pop();
    }
    this.forceUpdate();

    console.log("gallery:", galleryItem);
    console.log("state", this.state.pictures);
  }

  renderImage() {
    return (
      <View
        flexDirection="row"
        style={{
          height: Dimensions.get("window").height / 1.8,
          borderWidth: 0.5
        }}
      >
        {this.state.pictures.map((image, index) => {
          return (
            <View
              flexDirection="column"
              style={{
                width: Dimensions.get("window").width / 2
              }}
            >
              <View>
                <Image
                  style={{
                    width: Dimensions.get("window").width / 2 - 5,
                    height: Dimensions.get("window").width / 2 - 5,
                    resizeMode: "cover",
                    margin: 2.5
                  }}
                  source={image}
                />
              </View>
              <FormLabel labelStyle={{ fontSize: 13, color: "#2d3436" }}>
                Input Image ID
              </FormLabel>
              <FormInput
                inputStyle={{ color: "#2d3436" }}
                onChangeText={text => (this.state.images_id[index] = text)}
                onSubmitEditing={() =>
                  console.warn("images_id:",this.state.images_id[index])
                }
              />
              <FormLabel labelStyle={{ fontSize: 13, color: "#2d3436" }}>
                Input Image Name
              </FormLabel>
              <FormInput
                inputStyle={{ color: "#2d3436" }}
                onChangeText={text => (this.state.image_name[index] = text)}
                onSubmitEditing={() =>
                  console.warn(this.state.image_name[index])
                }
              />
            </View>
          );
        })}
      </View>
    );
  }

  // Submit item
  submitItem = async () => {
    let url = `https://ikapi.herokuapp.com/api/items/${this.params.data[0].id}`;

    const token = await AsyncStorage.getItem("token");
    console.warn("token", token);
    try {
      let formData = new FormData();
      formData.append("category_id", this.state.category_id);
      formData.append("item_name", this.state.item_name);
      formData.append("description", this.state.description);
      formData.append("specification", this.state.specification);
      formData.append("featured_image", this.state.featured_image);
      formData.append("featured_image_name", this.state.featured_image_name);
      for (let i = 0; i < this.state.item_details.length; i++) {
        for (let key in this.state.item_details[i]) {
          formData.append(
            `item_details[${i}][${key}]`,
            this.state.item_details[i][key]
          );
        }
      }

      for (let i = 0; i < this.state.pictures.length; i++) {
        formData.append("images", this.state.pictures[i]);
      }

      for (let i = 0; i < this.state.image_name.length; i++) {
        formData.append(`image_name[${i}]`, this.state.image_name[i]);
      }
      for (let i = 0; i < this.state.images_id.length; i++) {
        formData.append(`images_id[${i}]`, this.state.images_id[i]);
      }
      for (let i = 0; i < this.state.tags.length; i++) {
        formData.append(`tags[${i}]`, this.state.tags[i]);
      }
      console.warn("form:", formData);
      const fetchData = await fetch(url, {
        method: "PATCH",
        body: formData,
        headers: new Headers({
          "Content-Type": "multipart/form-data",
          Authorization: `Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImFkbWluQGdtYWlsLmNvbSIsImNyZWF0ZWRBdCI6IjIwMTgtMDgtMDJUMTM6MjQ6NTUuOTUzWiIsImlhdCI6MTUzMzIxNjI5NX0.W3h4jQr85Z_yA6W4xjvt_7TkbIwwL_klOL1voIOGPeE`
        })
      });
      console.warn("fetch:", fetchData);
      const errorFetch = JSON.parse(fetchData._bodyText);
      console.warn("error fetch: ", errorFetch);
      if (fetchData.status == 400) {
        Alert.alert(errorFetch.message);
        // this.setModal();
      } else {
        this.getAgain();
        this.props.navigation.navigate("FishList");
      }
    } catch (error) {
      console.log("Error1:", error);
    }
  };

  render() {
    // console.warn("state pictures", this.state.pictures);
    const { navigate } = this.props.navigation;
    // console.warn("ID TEST :", this.params.data.id);
    // console.warn("details", this.state.item_details);
    // console.warn("param", this.params.data[0]);
    console.warn("item details id :", this.params.data[0]);
    // console.warn("item details length :", this.state.length);
    // console.warn("item details id :", this.params.data[0].item_details.length);



    return (
      <View style={{ flex: 1 }}>
        <CommonHeader admin parent={this} />
        <View
          style={{
            justifyContent: "center",
            alignItems: "center"
          }}
        >
          <Text
            style={{
              color: "white",
              fontSize: 20,
              textShadowColor: "rgba(0, 0, 0, 0.75)",
              textShadowOffset: { width: -1, height: 1 },
              textShadowRadius: 10,
              marginVertical: 10
            }}
          >
            Edit fish list
          </Text>
        </View>
        <ScrollView>
          <FormLabel labelStyle={{ fontSize: 15, color: "#2d3436" }}>
            Fish Category ID
          </FormLabel>
          <FormInput
            inputStyle={{
              borderBottomColor: "#2d3436",
              borderBottomWidth: 1,
              color: "#2d3436"
            }}
            value={String(this.state.category_id)}
          />
          <FormLabel labelStyle={{ fontSize: 15, color: "#2d3436" }}>
            Fish Category
          </FormLabel>
          <FormInput
            inputStyle={{
              borderBottomColor: "#2d3436",
              borderBottomWidth: 1,
              color: "#2d3436"
            }}
            value={this.params.data[0].category.category_name}
          />
          <FormLabel labelStyle={{ fontSize: 15, color: "#2d3436" }}>
            Fish Name
          </FormLabel>
          <FormInput
            inputStyle={{
              borderBottomColor: "#2d3436",
              borderBottomWidth: 1,
              color: "#2d3436"
            }}
            onChangeText={text => this.updateItem(text, "itemName")}
            defaultValue={this.params.data[0].item_name}
          />

          <FormLabel labelStyle={{ fontSize: 15, color: "#2d3436" }}>
            Description
          </FormLabel>
          <TextInput
            multiline={true}
            numberOfLines={7}
            style={{
              borderWidth: 1,
              borderColor: "#2d3436",
              width: 320,
              marginLeft: 18
            }}
            onChangeText={text => this.updateDesc(text, "desc")}
            defaultValue={this.params.data[0].description}
          />
          <FormLabel labelStyle={{ fontSize: 15, color: "#2d3436" }}>
            Spesification
          </FormLabel>
          <FormInput
            inputStyle={{
              borderBottomColor: "#2d3436",
              borderBottomWidth: 1,
              color: "#2d3436"
            }}
            onChangeText={text => this.updateSpec(text, "spec")}
            defaultValue={this.params.data[0].specification}
          />

          <FormLabel labelStyle={{ fontSize: 15, color: "#2d3436" }}>
            Thumbnail Image Name
          </FormLabel>
          <FormInput
            inputStyle={{
              borderBottomColor: "#2d3436",
              borderBottomWidth: 1,
              color: "#2d3436"
            }}
            onChangeText={text => this.bgName(text, "bgName")}
          />

          <FormLabel labelStyle={{ fontSize: 15, color: "#2d3436" }}>
            Thumbnail Image
          </FormLabel>
          <FormLabel labelStyle={{ fontSize: 10, color: "grey", marginTop: 5 }}>
            max 1 image
          </FormLabel>
          <View style={{ flexDirection: "row" }}>
            <Button
              icon={{
                type: "entypo",
                name: "images",
                size: 15,
                color: "#45aaf2"
              }}
              onPress={() => this.pickSingle()}
              buttonStyle={{
                backgroundColor: "transparent",
                width: 70,
                height: 45,
                borderColor: "#45aaf2",
                borderWidth: 1,
                borderRadius: 5,
                marginTop: 10
              }}
            />
          </View>
          <View>
            {this.state.featured_image ? (
              this.renderThumbImage()
            ) : (
              <Text style={{ marginLeft: 15, marginTop: 5 }}>
                No selected Image
              </Text>
            )}
          </View>
          <FormLabel labelStyle={{ fontSize: 15, color: "#2d3436" }}>
            Fish Details
          </FormLabel>
          <Button
            icon={{
              type: "material-icons",
              name: "add",
              size: 15,
              color: "#45aaf2"
            }}
            onPress={() => this.variationItem()}
            buttonStyle={{
              backgroundColor: "transparent",
              width: 70,
              height: 45,
              borderColor: "#45aaf2",
              borderWidth: 1,
              borderRadius: 5,
              marginVertical: 20
            }}
          />
          {this.munculVariasi()}
          <FormLabel labelStyle={{ fontSize: 15, color: "#2d3436" }}>
            Tags
          </FormLabel>
          <TextInput
            style={{
              borderBottomColor: "#2d3436",
              borderBottomWidth: 1,
              color: "#2d3436",
              marginHorizontal: 17
            }}
            // onChangeText={}
            onSubmitEditing={event => this.inputTag(event.nativeEvent.text)}
          />
          <View style={{ flexDirection: "row" }}>{this.viewTag()}</View>

          <FormLabel labelStyle={{ fontSize: 15, color: "#2d3436" }}>
            Image
          </FormLabel>
          <FormLabel labelStyle={{ fontSize: 10, color: "grey", marginTop: 5 }}>
            max 4 image
          </FormLabel>
          <View style={{ flexDirection: "row" }}>
            <Button
              icon={{
                type: "entypo",
                name: "camera",
                size: 15,
                color: "#45aaf2"
              }}
              onPress={() => this.pickSingleWithCamera()}
              buttonStyle={{
                backgroundColor: "transparent",
                width: 70,
                height: 45,
                borderColor: "#45aaf2",
                borderWidth: 1,
                borderRadius: 5,
                marginVertical: 20
              }}
            />

            <Button
              icon={{
                type: "entypo",
                name: "images",
                size: 15,
                color: "#45aaf2"
              }}
              onPress={() => this.pickMultiple()}
              buttonStyle={{
                backgroundColor: "transparent",
                width: 70,
                height: 45,
                borderColor: "#45aaf2",
                borderWidth: 1,
                borderRadius: 5,
                marginVertical: 20
              }}
            />
          </View>
          {this.state.pictures.length > 0 ? (
            <ScrollView
              horizontal={true}
              // contentContainerStyle={{
              // height: Dimensions.get("window").height / 2
              // }}
            >
              <View
              // style={{ height: Dimensions.get("window").width / 8  }}
              >
                {this.renderImage()}
              </View>
            </ScrollView>
          ) : (
            <Text style={{ marginLeft: 15 }}>No selected Image</Text>
          )}
          <View style={{ justifyContent: "center", alignItems: "center" }}>
            <Button
              title="SUBMIT"
              onPress={() => this.submitItem()}
              buttonStyle={{
                backgroundColor: "#45aaf2",
                width: 150,
                height: 45,
                borderColor: "transparent",
                borderWidth: 1,
                borderRadius: 20,
                marginVertical: 20
              }}
            />
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5FCFF"
  },
  avatarContainer: {
    borderColor: "#9B9B9B",
    borderWidth: 1 / PixelRatio.get(),
    justifyContent: "center",
    alignItems: "center"
  },
  avatar: {
    borderRadius: 5,
    width: 150,
    height: 150
  }
});
