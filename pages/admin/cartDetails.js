import React, { Component } from "react";
import {
  View,
  Text,
  Image,
  ActivityIndicator,
  FlatList,
  AsyncStorage,
  TouchableOpacity,
  ImageBackground,
  ScrollView,
  Dimensions
} from "react-native";
import { SearchBar, Header, Icon } from "react-native-elements";
import CommonHeader from "../../assets/CommonHeader";

export default class cartDetails extends Component {
  constructor(props) {
    super(props);
    this.params = this.props.navigation.state.params;

    this.state = {
      dataSource: [],
      passJ: []
    };
  }

  static navigationOptions = {
    header: null,
    drawerLabel: "Cart"
  };

  searchChange() {
    return "sukses";
  }
  clearSearch() {
    return "hilang";
  }

  listItem() {
    let item1 = this.params.data.carts;
    return (
      <View>
        {item1.map(item2 => {
          return (
            <View>
              {item2.item_details.map(item3 => {
                return (
                  <View style={{ padding: 10 }}>
                    <Text>Fish name : {item3.item.item_name}</Text>
                    <Text>Size : {item3.size}</Text>
                    <Text>Quantity : {item2.quantity}</Text>
                  </View>
                );
              })}
            </View>
          );
        })}
      </View>
    );
  }

  render() {
    const { navigate } = this.props.navigation;
    console.warn("paramspar:", this.params.data.carts[0].item_details[0]);

    return (
      <View>
        <CommonHeader admin parent={this} />
        <View
          style={{
            justifyContent: "center",
            alignItems: "center"
          }}
        >
          <Text
            style={{
              color: "white",
              fontSize: 20,
              textShadowColor: "rgba(0, 0, 0, 0.75)",
              textShadowOffset: { width: -1, height: 1 },
              textShadowRadius: 10,
              marginTop: 10
            }}
          >
            Transaction Details
          </Text>
        </View>
        <ScrollView style={{ height: Dimensions.get("window").height - 150 }}>
          <View style={{ padding: 20, marginVertical: 5 }}>
            <View
              style={{
                borderRadius: 10,
                marginHorizontal: 5,
                marginBottom: 10,
                overflow: "hidden",
                shadowColor: "rgba(0, 0, 0, 0.75)",
                shadowOffset: { width: -1, height: 1 },
                shadowRadius: 10,
                elevation: 3
              }}
            >
              <Text
                style={{
                  backgroundColor: "#45aaf2",
                  fontSize: 15,
                  color: "white",
                  padding: 10
                }}
              >
                INFO DETAILS
              </Text>
              <Text style={{ padding: 10 }}>
                Status :{" "}
                {this.params.data.status ? (
                  <Text style={{ color: "red" }}> Pending</Text>
                ) : (
                  <Text style={{ color: "green" }}> Success</Text>
                )}
              </Text>
              <Text style={{ padding: 10 }}>
                Username : {this.params.data.user.name}
              </Text>
              <Text style={{ padding: 10 }}>
                Recipient Name :{" "}
                {this.params.data.shipping_address.recipient_name}
              </Text>
              <Text style={{ padding: 10 }}>
                Phone : {this.params.data.shipping_address.phone}
              </Text>
              <Text style={{ padding: 10 }}>
                Address : {this.params.data.shipping_address.address},{" "}
                {this.params.data.shipping_address.city},{" "}
                {this.params.data.shipping_address.province}
              </Text>
            </View>
            <View
              style={{
                borderRadius: 10,
                marginHorizontal: 5,
                marginBottom: 10,
                overflow: "hidden",
                shadowColor: "rgba(0, 0, 0, 0.75)",
                shadowOffset: { width: -1, height: 1 },
                shadowRadius: 10,
                elevation: 3
              }}
            >
              <Text
                style={{
                  backgroundColor: "#45aaf2",
                  fontSize: 15,
                  color: "white",
                  padding: 10
                }}
              >
                LIST ORDER
              </Text>
              {this.listItem()}
            </View>
            <View
              style={{
                borderRadius: 10,
                marginHorizontal: 5,
                marginBottom: 10,
                overflow: "hidden",
                shadowColor: "rgba(0, 0, 0, 0.75)",
                shadowOffset: { width: -1, height: 1 },
                shadowRadius: 10,
                elevation: 3
              }}
            >
              <Text
                style={{
                  backgroundColor: "#45aaf2",
                  fontSize: 15,
                  color: "white",
                  padding: 10
                }}
              >
                TOTAL : Rp{" "}
                {this.params.data.total_price
                  .toFixed(2)
                  .replace(".", ",")
                  .replace(/\d(?=(\d{3})+\,)/g, "$&.")}
              </Text>
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}
