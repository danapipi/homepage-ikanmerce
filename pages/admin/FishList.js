import React, { Component } from "react";
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  ActivityIndicator,
  FlatList,
  ImageBackground,
  AsyncStorage,
  Dimensions
} from "react-native";
import { SearchBar, Header, Button, Icon } from "react-native-elements";
import CommonHeader from "../../assets/CommonHeader";

export default class FishList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      show: "",
      dataSource: [],
      refreshing: false
    };
  }

  static navigationOptions = {
    header: null,
    drawerLabel: "Fish List Admin"
  };

  searchChange() {
    return "sukses";
  }
  clearSearch() {
    return "hilang";
  }

  async getIdItem(itemId) {
    let link = `https://ikapi.herokuapp.com/api/items/${itemId}`;
    console.warn(link);
    return fetch(link, {
      method: "GET",
      headers: new Headers({
        "Content-Type": "application/json",
        Authorization: `Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImFkbWluQGdtYWlsLmNvbSIsImNyZWF0ZWRBdCI6IjIwMTgtMDgtMDJUMTM6MjQ6NTUuOTUzWiIsImlhdCI6MTUzMzIxNjI5NX0.W3h4jQr85Z_yA6W4xjvt_7TkbIwwL_klOL1voIOGPeE`
      })
    });
  }

  async editListItem(itemId) {
    const editID = await this.getIdItem(itemId);

    const jsonP = JSON.parse(editID._bodyInit).data;
    this.setState({
      passJ: jsonP
    });
    console.warn("WADAW:", this.state.passJ);

    return this.props.navigation.navigate("editItem", {
      data: this.state.passJ
    });
  }

  removeItem(id) {
    let link = `https://ikapi.herokuapp.com/api/items/${id}`;
    console.warn("JALAN---", id);
    fetch(link, {
      method: "DELETE",
      headers: new Headers({
        "Content-Type": "application/json",
        Authorization: `Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImFkbWluQGdtYWlsLmNvbSIsImNyZWF0ZWRBdCI6IjIwMTgtMDgtMDJUMTM6MjQ6NTUuOTUzWiIsImlhdCI6MTUzMzIxNjI5NX0.W3h4jQr85Z_yA6W4xjvt_7TkbIwwL_klOL1voIOGPeE`
      })
    })
      .then(response => {
        const removeItem = this.state.dataSource.filter(item => {
          return item.id !== id;
        });
        this.setState({
          dataSource: removeItem
        });
        response.json().then(data => console.warn(data));
      })
      .catch(err => {
        console.log("error :" + err);
      });
    console.log("JALAN 2");
  }

  //    List Category
  renderItem = ({ item }) => {
    console.warn("item", item.id);
    return (
      <TouchableOpacity
        style={{
          borderRadius: 10,
          marginHorizontal: 15,
          marginBottom: 10,
          overflow: "hidden",
          shadowColor: "rgba(0, 0, 0, 0.75)",
          shadowOffset: { width: -1, height: 1 },
          shadowRadius: 10,
          elevation: 3,
          width: Dimensions.get("window").width - 30
          // height: 120
        }}
      >
        <ImageBackground
          style={{ padding: 10 }}
          resizeMode="cover"
          source={{
            uri: `https://ikapi.herokuapp.com/${item.featured_image}`
          }}
        >
          <View
            style={{
              flexDirection: "row"
            }}
          >
            <Text
              style={{
                color: "white",
                fontSize: 15,
                textShadowColor: "rgba(0, 0, 0, 0.75)",
                textShadowOffset: { width: -1, height: 1 },
                textShadowRadius: 10
              }}
            >
              {item.item_name}
            </Text>
            <View
              style={{
                flexDirection: "column",
                alignItems: "flex-end",
                alignContent: "flex-end",
                marginLeft: 80,
                flex: 1
              }}
            >
              <Button
                icon={{
                  type: "entypo",
                  name: "edit",
                  size: 15,
                  color: "white"
                }}
                title="Edit"
                onPress={() => this.editListItem(item.id)}
                buttonStyle={{
                  backgroundColor: "#f1c40f",
                  width: Dimensions.get("window").width / 4,
                  height: 45,
                  borderColor: "#f1c40f",
                  borderWidth: 0,
                  borderRadius: 20,
                  marginBottom: 10
                }}
              />
              <Button
                icon={{
                  type: "font-awesome",
                  name: "remove",
                  size: 15,
                  color: "white"
                }}
                title="Remove"
                onPress={() => this.removeItem(item.id)}
                buttonStyle={{
                  backgroundColor: "#e74c3c",
                  width: Dimensions.get("window").width / 4,
                  height: 45,
                  borderColor: "#e74c3c",
                  borderWidth: 0,
                  borderRadius: 20,
                  marginBottom: 10
                }}
              />
            </View>
          </View>
        </ImageBackground>
      </TouchableOpacity>
    );
  };

  handleRefresh = () =>{
    this.setState({refreshing: true}, ()=> this.componentDidMount())
  }

  async componentDidMount(){
    // const url = `https://ikapi.herokuapp.com/api/items/${itemId}?page=${pageNumber}&limit=${itemLimit}&cat=${categoryId}&tag=${tagID}&search=${searchWord}`;
    const url = `https://ikapi.herokuapp.com/api/items/`;

    try {
      const token = await AsyncStorage.getItem("token");
      // console.warn("TEST", token);
      // this.setState({ loading: true });

      try {
        const res = await fetch(url, {
          method: "GET",
          headers: new Headers({
            "Content-Type": "application/json",
            Authorization: `Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImFkbWluQGdtYWlsLmNvbSIsImNyZWF0ZWRBdCI6IjIwMTgtMDgtMDJUMTM6MjQ6NTUuOTUzWiIsImlhdCI6MTUzMzIxNjI5NX0.W3h4jQr85Z_yA6W4xjvt_7TkbIwwL_klOL1voIOGPeE`
          })
        });
        console.warn("res:", res);
        const data = await res.json();
        console.warn("data:", data.data);
        this.setState({
          dataSource: data.data,
          refreshing: false
        });
      } catch (error) {
        console.log(error);
      }
    } catch (error) {
      console.log(error);
    }
  }

  renderFooter() {
    return this.state.loading ? (
      <ActivityIndicator
        animating={this.state.loading}
        color="#bc2b78"
        size="large"
      />
    ) : null;
  }

  renderSeparator = () => {
    return <View />;
  };

  render() {
    const { navigate } = this.props.navigation;

    return (
      <View>
        <CommonHeader admin parent={this} />
        <View style={{ flexDirection: "row", height: 80 }}>
          <Button
            icon={{
              type: "materialicon",
              name: "add",
              size: 15,
              color: "white"
            }}
            title="Add Category"
            titleStyle={{}}
            onPress={() => this.props.navigation.navigate("addCategory")}
            buttonStyle={{
              flex: 1,
              backgroundColor: "#45aaf2",
              height: 45,
              borderColor: "#45aaf2",
              borderWidth: 1,
              borderRadius: 5,
              marginVertical: 20,
              elevation: 3,
              flex: 1
            }}
          />
          <Button
            icon={{
              type: "materialicon",
              name: "view-list",
              size: 15,
              color: "white"
            }}
            title="View Category"
            onPress={() => this.props.navigation.navigate("viewCategory")}
            buttonStyle={{
              flex: 1,
              backgroundColor: "#45aaf2",
              height: 45,
              borderColor: "#45aaf2",
              borderWidth: 1,
              borderRadius: 5,
              marginVertical: 20,
              elevation: 3
            }}
          />
        </View>
        <View
          style={{
            justifyContent: "center",
            alignItems: "center"
          }}
        >
          <View style={{ borderRadius: 5 }}>
            <Text
              style={{
                color: "white",
                fontSize: 20,
                textShadowColor: "rgba(0, 0, 0, 0.75)",
                textShadowOffset: { width: -1, height: 1 },
                textShadowRadius: 10,
                marginBottom: 10
              }}
            >
              FISH LIST
            </Text>
          </View>
          {this.state.dataSource.length > 0 ? (
            <View style={{ height: Dimensions.get("window").height - 240 }}>
              <FlatList
                data={this.state.dataSource}
                renderItem={this.renderItem}
                extraData={this.state}
                keyExtractor={(item, index) => index.toString()}
                ItemSeparatorComponent={this.renderSeparator}
                renderFooter={this.renderFooter.bind(this)}
                // onEndReached={this.end()}
                // onEndReachedThreshold={0.5}
                // refreshing={this.state.refreshing}
                // refreshControl={this.handleRefresh}
              />
            </View>
          ) : (
            <ActivityIndicator size="large" color="#45aaf2" />
          )}
        </View>
      </View>
    );
  }
}
