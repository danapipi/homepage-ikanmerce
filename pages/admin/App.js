import React, { Component } from "react";
import {
  StyleSheet,
  View,
  TouchableOpacity,
  ImageBackground,
  Image,
  Dimensions,
  ActivityIndicator
} from "react-native";
import { SearchBar, Header, Icon, Text } from "react-native-elements";
import CommonHeader from "../../assets/CommonHeader";
export default class App extends Component {
  constructor(props) {
    super(props);
  }

  static navigationOptions = {
    header: null,
    drawerLabel: "Home"
  };

  searchChange() {
    return "sukses";
  }
  clearSearch() {
    return "hilang";
  }

  render() {
    const { navigate } = this.props.navigation;

    return (
      <View>
        <CommonHeader admin parent={this} />
        {/* Slider */}
        <View
          style={{
            marginVertical: 10
          }}
        >
          <TouchableOpacity
            onPress={() => navigate("FishList")}
            style={{
              borderRadius: 10,
              marginHorizontal: 15,
              marginBottom: 10,
              overflow: "hidden",
              shadowColor: "rgba(0, 0, 0, 0.75)",
              shadowOffset: { width: -1, height: 1 },
              shadowRadius: 10,
              elevation: 3,
              width: Dimensions.get("window").width - 30,
              height: Dimensions.get("window").height / 6
            }}
          >
            <ImageBackground
              source={require("../../assets/ikan-hias.jpg")}
              style={{
                borderRadius: 5,
                height: Dimensions.get("window").height / 6
              }}
              resizeMode="cover"
              imageStyle={{ borderRadius: 10 }}
            >
              <Text
                style={{
                  color: "white",
                  paddingLeft: 20
                }}
                h4
              >
                Fishlist
              </Text>
            </ImageBackground>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => navigate("Cart")}
            style={{
              borderRadius: 10,
              marginHorizontal: 15,
              marginBottom: 10,
              overflow: "hidden",
              shadowColor: "rgba(0, 0, 0, 0.75)",
              shadowOffset: { width: -1, height: 1 },
              shadowRadius: 10,
              elevation: 3,
              width: Dimensions.get("window").width - 30,
              height: Dimensions.get("window").height / 6
            }}
          >
            <ImageBackground
              source={require("../../assets/ikan-predator.jpg")}
              style={{
                borderRadius: 5,
                height: Dimensions.get("window").height / 6
              }}
              resizeMode="cover"
              imageStyle={{ borderRadius: 10 }}
            >
              <Text
                style={{
                  color: "white",
                  paddingLeft: 20
                }}
                h4
              >
                Transaction History
              </Text>
            </ImageBackground>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}
