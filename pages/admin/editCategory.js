import React, { Component } from "react";
import {
  View,
  Text,
  ScrollView,
  FlatList,
  AsyncStorage,
  TouchableOpacity,
  Image,
  Dimensions,
  ActivityIndicator
} from "react-native";
import {
  SearchBar,
  Header,
  Icon,
  FormInput,
  FormLabel,
  Button
} from "react-native-elements";
import ImagePicker from "react-native-image-crop-picker";
import CommonHeader from "../../assets/CommonHeader";

export default class editCategory extends Component {
  constructor(props) {
    super(props);
    this.params = this.props.navigation.state.params;
    this.state = {
      old_name: "",
      category_name: "",
      category_image: null,
      show: "",
      dataSource: [],
      image: null,
      images: null,
      loading: false
    };
  }

  static navigationOptions = {
    header: null,
    drawerLabel: "Edit Category"
  };

  categoryInput = (text, field) => {
    if ((field = "categoryName")) {
      this.setState({
        category_name: text
      });
    }
  };

  // Gallery function
  pickSingle = async (cropit, circular = false) => {
    const image = await ImagePicker.openPicker({
      width: 300,
      height: 300,
      cropping: cropit,
      cropperCircleOverlay: circular,
      compressImageMaxWidth: 640,
      compressImageMaxHeight: 480,
      compressImageQuality: 0.5,
      includeExif: true
    });
    this.setState({
      category_image: {
        uri: image.path,
        type: image.mime,
        name: "apaaja.jpg"
      }
    });
  };

  renderImage() {
    return (
      <Image
        style={{
          width: Dimensions.get("window").width / 2 - 5,
          height: Dimensions.get("window").width / 2 - 5,
          resizeMode: "contain",
          margin: 2.5
        }}
        source={this.state.category_image}
      />
    );
  }

  // submit
  submitCategory = async () => {
    console.warn("-----", this.params.id.id);

    let url = `https://ikapi.herokuapp.com/api/categories/${this.params.id.id}`;
    try {
      const token = await AsyncStorage.getItem("token");

      try {
        const formData = new FormData();
        formData.append("category_name", this.state.category_name);
        formData.append("category_image", this.state.category_image);

        const fetchData = await fetch(url, {
          method: "PUT",
          body: formData,
          headers: new Headers({
            "Content-Type": "multipart/form-data",
            Authorization: `Bearer ${token}`
          })
        });
      } catch (error) {
        console.warn("Error edit1:", fetchData);
      }

      this.props.navigation.navigate("viewCategory");
      this.params.refresh();
    } catch (error) {
      console.warn("Error edit2:", error);
    }
  };

  render() {
    return (
      <ScrollView style={{ flex: 1 }}>
        <CommonHeader admin parent={this} />
        <FormLabel labelStyle={{ fontSize: 15, color: "#2d3436" }}>
          Old Fish Category
        </FormLabel>
        <FormInput
          inputStyle={{
            borderBottomColor: "#2d3436",
            borderBottomWidth: 1,
            color: "#2d3436"
          }}
          value={this.params.id.category_name}
        />
        <FormLabel labelStyle={{ fontSize: 15, color: "#2d3436" }}>
          New Fish Category
        </FormLabel>
        <FormInput
          inputStyle={{
            borderBottomColor: "#2d3436",
            borderBottomWidth: 1,
            color: "#2d3436"
          }}
          onChangeText={text => this.categoryInput(text, "categoryName")}
        />
        <FormLabel labelStyle={{ fontSize: 15, color: "#2d3436" }}>
          Background Image
        </FormLabel>
        <FormLabel labelStyle={{ fontSize: 10, color: "grey", marginTop: 5 }}>
          max 1 image
        </FormLabel>
        <View>
          <Button
            icon={{
              type: "entypo",
              name: "images",
              size: 15,
              color: "#45aaf2"
            }}
            onPress={() => this.pickSingle(false)}
            buttonStyle={{
              backgroundColor: "transparent",
              width: 70,
              height: 45,
              borderColor: "#45aaf2",
              borderWidth: 1,
              borderRadius: 5,
              marginVertical: 20
            }}
          />
          {this.state.category_image ? this.renderImage() : <Text />}
          <View />
          <View style={{ justifyContent: "center", alignItems: "center" }}>
            <Button
              title="SUBMIT"
              onPress={() => this.submitCategory()}
              buttonStyle={{
                backgroundColor: "#45aaf2",
                width: 150,
                height: 45,
                borderColor: "transparent",
                borderWidth: 1,
                borderRadius: 20,
                marginVertical: 20
              }}
            />
          </View>
          {/* {this.editCategoryI()} */}
        </View>
      </ScrollView>
    );
  }
}
