import React, { Component } from "react";
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  TextInput,
  ScrollView,
  StyleSheet,
  PixelRatio,
  Dimensions,
  AsyncStorage,
  Alert,
  Modal
} from "react-native";
import {
  SearchBar,
  Header,
  Icon,
  FormInput,
  FormLabel,
  Button
} from "react-native-elements";
import ImagePicker from "react-native-image-crop-picker";
import CommonHeader from "../../assets/CommonHeader";

export default class addFish extends Component {
  constructor(props) {
    super(props);
    this.params = this.props.navigation.state.params;
    this.state = {
      item_name: "",
      description: "",
      specification: "",
      item_details: [],
      tags: [],
      category: "",
      pictures: [],
      resultList: "",
      image: null,
      images: null,
      length: 0,
      category_id: "",
      featured_image: null,
      featured_image_name: "",
      image_name: [],
      dataSource: [],
      modalV: false
    };
  }

  static navigationOptions = {
    header: null,
    drawerLabel: "Add List"
  };

  // category_id
  categoryID() {
    console.log(this.params.data.id);
    this.setState({
      category_id: this.params.data.id
    });
  }

  componentDidMount() {
    this.categoryID();
  }

  // modal
  setModal(visible) {
    this.setState({
      modalV: visible
    });
  }

  // input product
  updateItem(text, field) {
    if ((field = "itemName")) {
      this.setState({
        item_name: text
      });
    }
  }

  //  input description
  updateDesc(text, field) {
    if ((field = "desc")) {
      this.setState({
        description: text
      });
    }
  }

  //  input spesification
  updateSpec(text, field) {
    if ((field = "spec")) {
      this.setState({
        specification: text
      });
    }
  }
  // input thumbnail image name
  bgName(text, field) {
    if ((field = "bgName")) {
      this.setState({
        featured_image_name: text
      });
    }
  }

  // get again
  async getAgain() {
    const url = `https://ikapi.herokuapp.com/api/items`;
    // console.warn("COMPONENT DID MOUNT:", url);

    try {
      AsyncStorage.getItem("token", (error, tokenLog) => {
        // console.warn("TEST");
        this.setState({ loading: false });

        fetch(url, {
          method: "GET",
          headers: new Headers({
            "Content-Type": "application/json",
            Authorization: "Bearer " + tokenLog
          })
        })
          .then(response => {
            // console.warn("RES IS RUNNING");
            console.log("response:", response);
            response.json().then(responseJson => {
              console.log("RUNNING", responseJson.data);
              this.setState({
                dataSource: responseJson.data,
                loading: false
              });
            });
          })
          .catch(error => {
            this.setState({ loading: true });
            console.log("COMPONENT MOUNT ERR : ", error);
          });
      });
    } catch (error) {
      console.log(error);
    }
  }

  //  input variation
  variationItem() {
    this.setState({ length: this.state.length + 1 });
    this.state.item_details.push({
      size: "",
      price: 0,
      discount: "",
      stock: 0
    });
  }

  munculVariasi() {
    return this.state.item_details.map((item, index) => {
      return (
        <View
          style={{
            borderColor: "#2d3436",
            borderRadius: 5,
            borderWidth: 1,
            margin: 15
          }}
        >
          <FormLabel>Size</FormLabel>
          <FormInput
            inputStyle={{ borderBottomColor: "#2d3436" }}
            onChangeText={text => (this.state.item_details[index].size = text)}
            onSubmitEditing={() => console.log(this.state.item_details[index])}
          />
          <FormLabel>Price</FormLabel>
          <FormInput
            onChangeText={text => (this.state.item_details[index].price = text)}
            onSubmitEditing={() => console.log(this.state.item_details[index])}
            keyboardType="numeric"
          />
          <FormLabel>Discount</FormLabel>
          <FormInput
            onChangeText={text =>
              (this.state.item_details[index].discount = text)
            }
            onSubmitEditing={() => console.log(this.state.item_details[index])}
            keyboardType="numeric"
          />
          <FormLabel>Stock</FormLabel>
          <FormInput
            onChangeText={text => (this.state.item_details[index].stock = text)}
            onSubmitEditing={() => console.log(this.state.item_details[index])}
            keyboardType="numeric"
          />
        </View>
      );
    });
  }

  // Tags
  inputTag(text) {
    this.state.tags.push(text);
    console.log("awas", this.state.tags);
    this.forceUpdate();
  }

  viewTag() {
    const arrView = [...this.state.tags];
    return arrView.map(item => {
      return (
        <View
          style={{
            borderColor: "transparent",
            borderRadius: 20,
            borderWidth: 1,
            height: 20,
            paddingHorizontal: 10,
            backgroundColor: "#ecf0f1",
            marginVertical: 10,
            elevation: 4,
            marginLeft: 15
          }}
        >
          <Text style={{ color: "#2d3436" }}>{item}</Text>
        </View>
      );
    });
  }

  // Search
  searchChange() {
    return "sukses";
  }
  clearSearch() {
    return "hilang";
  }

  // Camera function

  async pickSingleWithCamera(cropping) {
    const getCamera = await ImagePicker.openCamera({
      cropping: cropping,
      width: 200,
      height: 200,
      // includeExif: true,
      includeBase64: true
    });

    const imageItem = {
      uri: getCamera.path,
      type: getCamera.mime,
      name: "ikanlagi.jpg"
    };
    console.warn("cam:", imageItem);
    this.state.pictures.unshift(imageItem);

    if (this.state.pictures.length > 4) {
      this.state.pictures.pop();
    }
    this.forceUpdate();
  }

  // Gallery single
  pickSingle = async (cropit, circular = false) => {
    const image = await ImagePicker.openPicker({
      width: 300,
      height: 300,
      cropping: cropit,
      cropperCircleOverlay: circular,
      compressImageMaxWidth: 640,
      compressImageMaxHeight: 480,
      compressImageQuality: 0.5,
      includeExif: true
    });
    this.setState({
      featured_image: {
        uri: image.path,
        type: image.mime,
        name: "apaaja.jpg"
      }
    });

    // console.warn("eroer", this.state.category_image);
  };

  renderThumbImage() {
    return (
      <Image
        style={{
          width: Dimensions.get("window").width / 2 - 5,
          height: Dimensions.get("window").width / 2 - 5,
          resizeMode: "contain",
          marginVertical: 4.5,
          marginLeft: 15
        }}
        source={this.state.featured_image}
      />
    );
  }

  // Gallery function
  async pickMultiple() {
    const galleryItem = await ImagePicker.openPicker({
      multiple: true,
      waitAnimationEnd: false,
      // includeExif: true,
      includeBase64: true,
      compressImageMaxWidth: 100,
      compressImageMaxHeight: 100,
      // compressImageQuality: 0.8,
      enableRotationGesture: true
    });
    console.log("ok",Array.isArray( galleryItem));
    const imageItem = galleryItem.map((item)=>{return{
      uri: item.path,
      type: item.mime,
      name: "ikangaler.jpg"
      }})
    console.log("image item: ", imageItem);
    this.state.pictures=[...imageItem,... this.state.pictures];
    if (this.state.pictures.length > 4) {
      this.state.pictures.pop();
    }
    this.forceUpdate();

    console.log("gallery:", galleryItem);
    console.log("state", this.state.pictures);
  }

  renderImage() {
    return (
      <View
        flexDirection="row"
        style={{
          height: Dimensions.get("window").height / 2.5,
          borderWidth: 0.5
        }}
      >
        {this.state.pictures.map((image, index) => {
          return (
            <View
              flexDirection="column"
              style={{
                width: Dimensions.get("window").width / 2
              }}
            >
              <View>
                <Image
                  style={{
                    width: Dimensions.get("window").width / 2 - 5,
                    height: Dimensions.get("window").width / 2 - 5,
                    resizeMode: "cover",
                    margin: 2.5
                  }}
                  source={image}
                />
              </View>
              <FormLabel labelStyle={{ fontSize: 13, color: "#2d3436" }}>
                Input Image Name
              </FormLabel>
              <FormInput
                inputStyle={{ color: "#2d3436" }}
                onChangeText={text => (this.state.image_name[index] = text)}
                onSubmitEditing={() =>
                  console.log(this.state.image_name[index])
                }
              />
            </View>
          );
        })}
      </View>
    );
  }

  // Submit item
  submitItem = async () => {
    let url = `https://ikapi.herokuapp.com/api/items/`;

    const token = await AsyncStorage.getItem("token");
    console.log("token", token);
    try {
      let formData = new FormData();
      formData.append("category_id", this.state.category_id);
      formData.append("item_name", this.state.item_name);
      formData.append("description", this.state.description);
      formData.append("specification", this.state.specification);
      formData.append("featured_image", this.state.featured_image);
      formData.append("featured_image_name", this.state.featured_image_name);

      for (let i = 0; i < this.state.item_details.length; i++) {
        for (let key in this.state.item_details[i]) {
          formData.append(
            `item_details[${i}][${key}]`,
            this.state.item_details[i][key]
          );
        }
      }

      for (let i = 0; i < this.state.pictures.length; i++) {
        formData.append("images", this.state.pictures[i]);
      }

      for (let i = 0; i < this.state.image_name.length; i++) {
        formData.append(`image_name[${i}]`, this.state.image_name[i]);
      }
      for (let i = 0; i < this.state.tags.length; i++) {
        formData.append(`tags[${i}]`, this.state.tags[i]);
      }

      console.log("form:", formData);
      const fetchData = await fetch(url, {
        method: "POST",
        body: formData,
        headers: new Headers({
          "Content-Type": "multipart/form-data",
          Authorization: `Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImFkbWluQGdtYWlsLmNvbSIsImNyZWF0ZWRBdCI6IjIwMTgtMDgtMDJUMTM6MjQ6NTUuOTUzWiIsImlhdCI6MTUzMzIxNjI5NX0.W3h4jQr85Z_yA6W4xjvt_7TkbIwwL_klOL1voIOGPeE`
        })
      });
      const errorFetch = JSON.parse(fetchData._bodyText);
      if (fetchData.status == 400) {
        Alert.alert(errorFetch.message);
        // this.setModal();
      } else {
        this.getAgain();
        this.props.navigation.navigate("FishList");
      }
    } catch (error) {
      console.log(error);
    }
  };

  render() {
    // console.warn("state pictures", this.state.pictures);
    const { navigate } = this.props.navigation;
    // console.warn("ID TEST :", this.params.data.id);
    // console.warn("details", this.state.item_details);

    return (
      <View style={{ flex: 1 }}>
        <CommonHeader admin parent={this}/>
        <View
          style={{
            justifyContent: "center",
            alignItems: "center"
          }}
        >
          <Text
            style={{
              color: "white",
              fontSize: 20,
              textShadowColor: "rgba(0, 0, 0, 0.75)",
              textShadowOffset: { width: -1, height: 1 },
              textShadowRadius: 10,
              marginVertical: 10
            }}
          >
            Add Fish List
          </Text>
        </View>
        <ScrollView>
          <FormLabel labelStyle={{ fontSize: 15, color: "#2d3436" }}>
            Fish Category ID
          </FormLabel>
          <FormInput
            inputStyle={{
              borderBottomColor: "#2d3436",
              borderBottomWidth: 1,
              color: "#2d3436"
            }}
            defaultValue={String(this.state.category_id)}
          />
          <FormLabel labelStyle={{ fontSize: 15, color: "#2d3436" }}>
            Fish Category
          </FormLabel>
          <FormInput
            inputStyle={{
              borderBottomColor: "#2d3436",
              borderBottomWidth: 1,
              color: "#2d3436"
            }}
            value={this.params.data.category_name}
          />
          <FormLabel labelStyle={{ fontSize: 15, color: "#2d3436" }}>
            Fish Name
          </FormLabel>
          <FormInput
            inputStyle={{
              borderBottomColor: "#2d3436",
              borderBottomWidth: 1,
              color: "#2d3436"
            }}
            onChangeText={text => this.updateItem(text, "itemName")}
          />

          <FormLabel labelStyle={{ fontSize: 15, color: "#2d3436" }}>
            Description
          </FormLabel>
          <TextInput
            multiline={true}
            numberOfLines={7}
            style={{
              borderWidth: 1,
              borderColor: "#2d3436",
              width: 320,
              marginLeft: 18
            }}
            onChangeText={text => this.updateDesc(text, "desc")}
          />
          <FormLabel labelStyle={{ fontSize: 15, color: "#2d3436" }}>
            Spesification
          </FormLabel>
          <FormInput
            inputStyle={{
              borderBottomColor: "#2d3436",
              borderBottomWidth: 1,
              color: "#2d3436"
            }}
            onChangeText={text => this.updateSpec(text, "spec")}
          />

          <FormLabel labelStyle={{ fontSize: 15, color: "#2d3436" }}>
            Thumbnail Image Name
          </FormLabel>
          <FormInput
            inputStyle={{
              borderBottomColor: "#2d3436",
              borderBottomWidth: 1,
              color: "#2d3436"
            }}
            onChangeText={text => this.bgName(text, "bgName")}
          />

          <FormLabel labelStyle={{ fontSize: 15, color: "#2d3436" }}>
            Thumbnail Image
          </FormLabel>
          <FormLabel labelStyle={{ fontSize: 10, color: "grey", marginTop: 5 }}>
            max 1 image
          </FormLabel>
          <View style={{ flexDirection: "row" }}>
            <Button
              icon={{
                type: "entypo",
                name: "images",
                size: 15,
                color: "#45aaf2"
              }}
              onPress={() => this.pickSingle()}
              buttonStyle={{
                backgroundColor: "transparent",
                width: 70,
                height: 45,
                borderColor: "#45aaf2",
                borderWidth: 1,
                borderRadius: 5,
                marginTop: 10
              }}
            />
          </View>
          <View>
            {this.state.featured_image ? (
              this.renderThumbImage()
            ) : (
              <Text style={{ marginLeft: 15, marginTop: 5 }}>
                No selected Image
              </Text>
            )}
          </View>

          <FormLabel labelStyle={{ fontSize: 15, color: "#2d3436" }}>
            Fish Details
          </FormLabel>
          <Button
            icon={{
              type: "material-icons",
              name: "add",
              size: 15,
              color: "#45aaf2"
            }}
            onPress={() => this.variationItem()}
            buttonStyle={{
              backgroundColor: "transparent",
              width: 70,
              height: 45,
              borderColor: "#45aaf2",
              borderWidth: 1,
              borderRadius: 5,
              marginVertical: 20
            }}
          />
          {this.munculVariasi()}
          <FormLabel labelStyle={{ fontSize: 15, color: "#2d3436" }}>
            Tags
          </FormLabel>
          <TextInput
            style={{
              borderBottomColor: "#2d3436",
              borderBottomWidth: 1,
              color: "#2d3436",
              marginHorizontal: 17
            }}
            // onChangeText={}
            onSubmitEditing={event => this.inputTag(event.nativeEvent.text)}
          />
          <View style={{ flexDirection: "row" }}>
            {this.state.tags.length > 0 ? (
              this.viewTag()
            ) : (
              <Text style={{ paddingLeft: 15, marginVertical: 10 }}>
                No tags input
              </Text>
            )}
          </View>

          <FormLabel labelStyle={{ fontSize: 15, color: "#2d3436" }}>
            Image
          </FormLabel>
          <FormLabel labelStyle={{ fontSize: 10, color: "grey", marginTop: 5 }}>
            max 4 image
          </FormLabel>
          <View style={{ flexDirection: "row" }}>
            <Button
              icon={{
                type: "entypo",
                name: "camera",
                size: 15,
                color: "#45aaf2"
              }}
              onPress={() => this.pickSingleWithCamera()}
              buttonStyle={{
                backgroundColor: "transparent",
                width: 70,
                height: 45,
                borderColor: "#45aaf2",
                borderWidth: 1,
                borderRadius: 5,
                marginVertical: 20
              }}
            />

            <Button
              icon={{
                type: "entypo",
                name: "images",
                size: 15,
                color: "#45aaf2"
              }}
              onPress={() => this.pickMultiple()}
              buttonStyle={{
                backgroundColor: "transparent",
                width: 70,
                height: 45,
                borderColor: "#45aaf2",
                borderWidth: 1,
                borderRadius: 5,
                marginVertical: 20
              }}
            />
          </View>
          {this.state.pictures.length > 0 ? (
            <ScrollView
              horizontal={true}
              // contentContainerStyle={{
              // height: Dimensions.get("window").height / 2
              // }}
            >
              <View
              // style={{ height: Dimensions.get("window").width / 8  }}
              >
                {this.renderImage()}
              </View>
            </ScrollView>
          ) : (
            <Text style={{ marginLeft: 15 }}>No selected Image</Text>
          )}
          <View style={{ justifyContent: "center", alignItems: "center" }}>
            <Button
              title="SUBMIT"
              onPress={() => this.submitItem()}
              buttonStyle={{
                backgroundColor: "#45aaf2",
                width: 150,
                height: 45,
                borderColor: "transparent",
                borderWidth: 1,
                borderRadius: 20,
                marginVertical: 20
              }}
            />
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5FCFF"
  },
  avatarContainer: {
    borderColor: "#9B9B9B",
    borderWidth: 1 / PixelRatio.get(),
    justifyContent: "center",
    alignItems: "center"
  },
  avatar: {
    borderRadius: 5,
    width: 150,
    height: 150
  }
});
