import React, { Component } from "react";
import {
  View,
  Text,
  ScrollView,
  FlatList,
  AsyncStorage,
  TouchableOpacity,
  Image,
  Dimensions,
  ActivityIndicator,
  ImageBackground
} from "react-native";
import {
  SearchBar,
  Header,
  Icon,
  FormInput,
  FormLabel,
  Button
} from "react-native-elements";
import ImagePicker from "react-native-image-crop-picker";
import CommonHeader from "../../assets/CommonHeader";

export default class viewCategory extends Component {
  constructor(props) {
    super(props);
    this.state = {
      category_name: "",
      category_image: null,
      dataSource: [],
      image: null,
      images: null,
      loading: false,
      passJ: []
    };
  }

  static navigationOptions = {
    header: null
  };

  // refresh f
  refresh() {
    this.forceUpdate();
  }

  async getId(id) {
    let link = `https://ikapi.herokuapp.com/api/categories/${id}`;
    console.warn(link);
    return fetch(link, {
      method: "GET",
      headers: new Headers({
        "Content-Type": "application/json",
        Authorization: `Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImFkbWluQGdtYWlsLmNvbSIsImNyZWF0ZWRBdCI6IjIwMTgtMDgtMDJUMTM6MjQ6NTUuOTUzWiIsImlhdCI6MTUzMzIxNjI5NX0.W3h4jQr85Z_yA6W4xjvt_7TkbIwwL_klOL1voIOGPeE`
      })
    });
  }

  async getAgain() {
    const url = `https://ikapi.herokuapp.com/api/categories`;
    // console.warn("COMPONENT DID MOUNT:", url);

    try {
      AsyncStorage.getItem("token", (error, tokenLog) => {
        // console.warn("TEST");
        this.setState({ loading: false });

        fetch(url, {
          method: "GET",
          headers: new Headers({
            "Content-Type": "application/json",
            Authorization: "Bearer " + tokenLog
          })
        })
          .then(response => {
            // console.warn("RES IS RUNNING");
            console.log("response:", response);
            response.json().then(responseJson => {
              // console.warn("RUNNING", responseJson.data);
              this.setState({
                dataSource: responseJson.data,
                loading: false
              });
            });
          })
          .catch(error => {
            this.setState({ loading: true });
            console.log("COMPONENT MOUNT ERR : ", error);
          });
      });
    } catch (error) {
      console.log(error);
    }
  }

  async editList(id) {
    const editID = await this.getId(id);

    const jsonP = JSON.parse(editID._bodyInit).data;
    this.setState({
      passJ: jsonP
    });
    console.warn("WADAW:", this.state.passJ);

    return this.props.navigation.navigate("editCategory", {
      id: this.state.passJ,
      refresh: this.refresh
    });
  }

  async touch(id) {
    const touchId = await this.getId(id);

    const jsonP = JSON.parse(touchId._bodyInit).data;
    this.setState({
      passJ: jsonP
    });
    return this.props.navigation.navigate("addFish", { data: jsonP });
  }

  removeList(id) {
    let link = `https://ikapi.herokuapp.com/api/categories/${id}`;
    console.warn("JALAN---", id);
    fetch(link, {
      method: "DELETE",
      headers: new Headers({
        "Content-Type": "application/json",
        Authorization: `Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImFkbWluQGdtYWlsLmNvbSIsImNyZWF0ZWRBdCI6IjIwMTgtMDgtMDJUMTM6MjQ6NTUuOTUzWiIsImlhdCI6MTUzMzIxNjI5NX0.W3h4jQr85Z_yA6W4xjvt_7TkbIwwL_klOL1voIOGPeE`
      })
    })
      .then(response => {
        const removeItem = this.state.dataSource.filter(item => {
          return item.id !== id;
        });
        this.setState({
          dataSource: removeItem
        });
        response.json().then(data => console.warn(data));
      })
      .catch(err => {
        console.warn("error :" + err);
      });
    console.warn("JALAN 2");
  }

  //   List Category
  renderItem = ({ item }) => {
    console.warn(item.id, item.category_image);
    return (
      <TouchableOpacity
        style={{
          borderRadius: 10,
          marginHorizontal: 15,
          marginBottom: 10,
          overflow: "hidden",
          shadowColor: "rgba(0, 0, 0, 0.75)",
          shadowOffset: { width: -1, height: 1 },
          shadowRadius: 10,
          elevation: 3
        }}
        onPress={() => this.touch(item.id)}
      >
        <ImageBackground
          style={{ padding: 10 }}
          resizeMode={"cover"}
          source={{ uri: `https://ikapi.herokuapp.com/${item.category_image}` }}
        >
          <View
            style={{
              flexDirection: "row"
            }}
          >
            <Text
              style={{
                color: "white",
                fontSize: 15,
                textShadowColor: "rgba(0, 0, 0, 0.75)",
                textShadowOffset: { width: -1, height: 1 },
                textShadowRadius: 10
              }}
            >
              {item.category_name}
            </Text>
            <View
              style={{
                flexDirection: "column",
                alignItems: "flex-end",
                alignContent: "flex-end",
                marginLeft: 80,
                flex: 1
              }}
            >
              <Button
                icon={{
                  type: "entypo",
                  name: "edit",
                  size: 15,
                  color: "white"
                }}
                title="Edit"
                onPress={() => this.editList(item.id)}
                buttonStyle={{
                  backgroundColor: "#f1c40f",
                  width: 100,
                  height: 45,
                  borderColor: "#f1c40f",
                  borderWidth: 0,
                  borderRadius: 20,
                  marginBottom: 10
                }}
              />
              <Button
                icon={{
                  type: "font-awesome",
                  name: "remove",
                  size: 15,
                  color: "white"
                }}
                title="Remove"
                onPress={() => this.removeList(item.id)}
                buttonStyle={{
                  backgroundColor: "#e74c3c",
                  width: 100,
                  height: 45,
                  borderColor: "#e74c3c",
                  borderWidth: 0,
                  borderRadius: 20
                }}
              />
            </View>
          </View>
        </ImageBackground>
      </TouchableOpacity>
    );
    // console.warn(item.category_image);
  };

  async componentDidMount() {
    const url = `https://ikapi.herokuapp.com/api/categories`;
    // console.warn("COMPONENT DID MOUNT:", url);

    try {
      AsyncStorage.getItem("token", (error, tokenLog) => {
        // console.warn("TEST");
        this.setState({ loading: false });

        fetch(url, {
          method: "GET",
          headers: new Headers({
            "Content-Type": "application/json",
            Authorization: "Bearer " + tokenLog
          })
        })
          .then(response => {
            // console.warn("RES IS RUNNING");
            console.log("response:", response);
            response.json().then(responseJson => {
              // console.warn("RUNNING", responseJson.data);
              this.setState({
                dataSource: responseJson.data,
                loading: false
              });
            });
          })
          .catch(error => {
            this.setState({ loading: true });
            console.log("COMPONENT MOUNT ERR : ", error);
          });
      });
    } catch (error) {
      console.log(error);
    }
  }

  renderFooter() {
    return this.state.loading ? (
      <ActivityIndicator
        animating={this.state.loading}
        color="#bc2b78"
        size="large"
      />
    ) : null;
  }

  renderSeparator = () => {
    return <View />;
  };

  render() {
    return (
      <ScrollView style={{ flex: 1 }}>
        <CommonHeader admin parent={this} />
        <View
          style={{
            justifyContent: "center",
            alignItems: "center"
          }}
        >
          <Text
            style={{
              color: "white",
              fontSize: 20,
              textShadowColor: "rgba(0, 0, 0, 0.75)",
              textShadowOffset: { width: -1, height: 1 },
              textShadowRadius: 10,
              marginVertical: 10
            }}
          >
            Fish Categories
          </Text>
        </View>
        <View>
          {this.state.dataSource.length > 0 ? (
            <FlatList
              data={this.state.dataSource}
              renderItem={this.renderItem}
              extraData={this.state}
              keyExtractor={(item, index) => index.toString()}
              ItemSeparatorComponent={this.renderSeparator}
              renderFooter={this.renderFooter.bind(this)}
            />
          ) : (
            <ActivityIndicator size="large" color="#45aaf2" />
          )}
        </View>
      </ScrollView>
    );
  }
}
