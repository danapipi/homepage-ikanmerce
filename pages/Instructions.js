import React, { Component } from "react";
import { AppRegistry, Alert, Dimensions } from "react-native";
import AppIntro from "react-native-app-intro";

export default class Instructions extends Component {
  static navigationOptions = {
    header: null
  };

  onSkipBtnHandle = index => {
    this.props.navigation.navigate("Login");
  };
  doneBtnHandle = () => {
    // Alert.alert('Done');
    this.props.navigation.navigate("Login");
  };
  nextBtnHandle = index => {
    Alert.alert("Next");
    console.log(index);
  };
  onSlideChangeHandle = (index, total) => {
    console.log(index, total);
  };
  render() {
    const pageArray = [
      {
        title: "Make an account",
        img: require("../assets/registe.jpg"),
        imgStyle: {
          marginTop: 100,
          // height: 170,
          height: Dimensions.get("window").width -10,
          // width: 120,
          width: Dimensions.get("window").width - 100,
          borderRadius: 10
        },
        backgroundColor: "#45aaf2",
        fontColor: "#fff",
        level: 10
      },
      {
        title: "Login",
        img: require("../assets/logine.jpg"),
        imgStyle: {
          marginTop: 60,
          height: 170,
          width: 120,
          borderRadius: 10
        },
        backgroundColor: "#4b7bec",
        fontColor: "#fff",
        level: 15
      },
      {
        title: "Choose products",
        img: require("../assets/adde.jpg"),
        imgStyle: {
          marginTop: 60,
          height: 210,
          width: 270,
          borderRadius: 10
        },
        backgroundColor: "#2bcbba",
        fontColor: "#fff",
        level: 15
      },
      {
        title: "Add products",
        img: require("../assets/carte.jpg"),
        imgStyle: {
          marginTop: 60,
          height: 100,
          width: 300,
          borderRadius: 10
        },
        backgroundColor: "#3867d6",
        fontColor: "#fff",
        level: 15
      },
      {
        title: "Checkout cart ",
        img: require("../assets/checkout1e.jpg"),
        imgStyle: {
          marginTop: 60,
          height: 100,
          width: 290,
          borderRadius: 10
        },
        backgroundColor: "#2d98da",
        fontColor: "#fff",
        level: 15
      },
      {
        title: "Enjor your fish",
        imgStyle: {
          height: 93 * 2.5,
          width: 103 * 2.5
        },
        backgroundColor: "#45aaf2",
        fontColor: "#fff",
        level: 10
      }
    ];
    return (
      <AppIntro
        onNextBtnClick={this.nextBtnHandle}
        onDoneBtnClick={this.doneBtnHandle}
        onSkipBtnClick={this.onSkipBtnHandle}
        onSlideChange={this.onSlideChangeHandle}
        pageArray={pageArray}
      />
    );
  }
}
