import React, { Component } from "react";
import {
  Button,
  View,
  ImageBackground,
  TextInput,
  AsyncStorage,
  ActivityIndicator,
  Text,
  Alert,
  Image
} from "react-native";
import Axios from "axios";

export default class LoginScreen extends Component {
  constructor(props) {
    super(props);

    if (typeof global.self === "undefined") {
      global.self = global;
    }
    this.state = {
      email: "",
      password: "",
      load: true
    };
  }
  static navigationOptions = {
    header: null
  };

  render() {
    if (this.state.load)
      return (
        <ImageBackground
          source={{
            uri:
              "https://images.unsplash.com/photo-1459207982041-089ff95be891?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=aae95477e97e6cf27c8eee213ba9dbf5&auto=format&fit=crop&w=634&q=80"
          }}
          style={{ flex: 1 }}
        >
          <ActivityIndicator color="white" size={50} />
        </ImageBackground>
      );
    else
      return (
        <View style={{ flex: 1 }}>
          <View style={{ flex: 1 }} />
          <View style={{ margin: 30 }}>
            <View
              style={{
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <Image source={require("../assets/ikanlogo.png")} />
            </View>
            <TextInput
              style={{
                backgroundColor: "white",
                borderColor: "lightgrey",
                borderRadius: 3
              }}
              returnKeyType={"next"}
              onSubmitEditing={() => {
                this.secondTextInput.focus();
              }}
              blurOnSubmit={false}
              placeholder="Email"
              onChangeText={email => this.setState({ email })}
            />
            <TextInput
              style={{
                backgroundColor: "white",
                borderColor: "lightgrey",
                borderRadius: 3,
                marginTop: 8
              }}
              secureTextEntry={true}
              ref={input => {
                this.secondTextInput = input;
              }}
              onSubmitEditing={this.onPressLogin.bind(this)}
              placeholder="Password"
              onChangeText={password => this.setState({ password })}
            />
            <View style={{ height: 10 }} />
            <Text onPress={this.onPressForget.bind(this)}>
              Forgot password?
            </Text>
            <View style={{ height: 10 }} />
            <Button title="SIGN IN" onPress={this.onPressLogin.bind(this)} />
            <View style={{ height: 10 }} />
            <Button title="SIGN UP" onPress={this.onPressReg.bind(this)} />
            <View style={{ height: 10 }} />
            <Button
              color="orange"
              title="SIGN IN WITH GOOGLE"
              onPress={this.onPressReg.bind(this)}
            />
          </View>
        </View>
      );
  }

  onPressReg() {
    this.props.navigation.navigate("Register");
  }

  onPressForget() {
    this.props.navigation.navigate("Forget");
  }

  onPressLogin() {
    const self = this;
    console.log("LOGGING IN");

    Axios("https://ikapi.herokuapp.com/api/users/login", {
      method: "POST",
      data: JSON.stringify({
        email: this.state.email,
        password: this.state.password
      }),
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(response => {
        console.log("LOGGING IN 2", response.data);
        AsyncStorage.setItem("token", response.data.authorization, err =>
          console.log("token : ", response.data.authorization, err)
        );

        if (response.data.success === true) {
          if (response.data.is_admin) this.props.navigation.replace("Admin");
          else this.props.navigation.replace("User");
        }
      })
      .catch(err => {
        Alert.alert("ERROR", JSON.stringify(err));
        console.log("ERROR", err);
      });
  }

  async componentWillMount() {
    const self = this;
    console.log("LOGIN WILL MOUNT");
    const token = await AsyncStorage.getItem("token");
    console.log("TOKEN MURAH MERIAH", token);
    if (token)
      Axios("https://ikapi.herokuapp.com/api/users/check", {
        method: "GET",
        headers: {
          Authorization: "Bearer " + token
        }
      })
        .then(response => {
          console.log(response.data);
          self.setState({ load: false });

          if (response.data.success === true) {
            if (response.data.is_admin) this.props.navigation.replace("Admin");
            else this.props.navigation.replace("User");
          }
        })
        .catch(err => {
          self.setState({ load: false });
          console.log("Err : ", err);
        });
    else self.setState({ load: false });
  }
}
