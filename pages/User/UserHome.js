import React, { Component } from "react";
import {
  View,
  TouchableOpacity,
  ImageBackground,
  Dimensions,
  ActivityIndicator,
  ScrollView,
  FlatList
} from "react-native";
import { SearchBar, Header, Icon, Text } from "react-native-elements";
import Axios from "axios";
import CommonHeader from "../../assets/CommonHeader";
import { NavigationActions } from "react-navigation";

export default class UserHome extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cats: []
    };
  }

  static navigationOptions = {
    header: null,
    drawerLabel: "Home"
  };

  searchChange() {
    return "sukses";
  }
  clearSearch() {
    return "hilang";
  }

  componentDidMount() {
    Axios(`https://ikapi.herokuapp.com/api/categories/`).then(response => {
      this.setState({ cats: response.data.data });
    });
  }

  renderyItem = ({ item }) => (
    <TouchableOpacity
      key={item.id}
      style={{
        width: Dimensions.get("window").width - 20,
        marginHorizontal: 10,
        height: Dimensions.get("window").height / 6 - 5,
        backgroundColor: "white",
        elevation: 4,
        overflow: "hidden",
        marginVertical: 5,
        borderRadius: 5
      }}
      onPress={() => {
        const navAction = NavigationActions.navigate({
          routeName: "Fish List",
          action: NavigationActions.navigate({
            routeName: "FishList",
            params: { category: item.id }
          })
        });
        this.props.navigation.dispatch(navAction);
      }}
    >
      <ImageBackground
        style={{
          width: Dimensions.get("window").width - 20,
          height: Dimensions.get("window").height / 6 - 5
        }}
        source={{
          uri: "https://ikapi.herokuapp.com/" + item.category_image
        }}
        resizeMode="cover"
      >
        <Text
          style={{
            fontSize: 40,
            margin: 20,
            color: "white",
            fontWeight: "bold"
          }}
        >
          {item.category_name}
        </Text>
      </ImageBackground>
    </TouchableOpacity>
  );

  render() {
    const navigation = this.props.navigation;
    return (
      <View>
        <CommonHeader parent={this} />
        {this.state.cats.length === 0 ? (
          <View
            style={{
              height: Dimensions.get("window").height - 95,
              alignContent: "center",
              justifyContent: "center"
            }}
          >
            <ActivityIndicator size="large" />
          </View>
        ) : (
          <FlatList
            style={{
              height: Dimensions.get("window").height - 95
            }}
            data={this.state.cats}
            extraData={this.state}
            renderItem={this.renderyItem}
            keyExtractor={(item, index) => String(index)}
          />
        )}
      </View>
    );
  }
}
