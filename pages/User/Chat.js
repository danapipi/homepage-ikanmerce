import React, { Component } from "react";
import { View, Text, Image } from "react-native";
import { SearchBar, Header, Icon } from "react-native-elements";

export default class Chat extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  static navigationOptions = {
    header: null,
    drawerLabel: "Chat"
  };

  searchChange() {
    return "sukses";
  }
  clearSearch() {
    return "hilang";
  }

  render() {
    const { navigate } = this.props.navigation;

    return (
      <View>
        <Header
          innerContainerStyles={{
            flex: 1,
            flexDirection: "row",
            justifyContent: "space-between",
            alignItems: "center"
          }}
          leftComponent={
            <Icon
              name="menu"
              color="#fff"
              onPress={() => this.props.navigation.openDrawer()}
            />
          }
          centerComponent={
            <SearchBar
              round
              lightTheme
              containerStyle={{
                width: 230,
                backgroundColor: "transparent",
                borderTopWidth: 0,
                borderBottomWidth: 0
              }}
              inputStyle={{ backgroundColor: "white" }}
              onChangeText={this.searchChange}
              onClearText={this.clearSearch}
              icon={{ type: "font-awesome", name: "search" }}
              placeholder="Search fish here..."
            />
          }
          rightComponent={
            <Icon
              name="fish"
              color="#fff"
              type="material-community"
              size={30}
              onPress={() => navigate("Cart")}
            />
          }
          backgroundColor="#45aaf2"
        />
        <View
          style={{
            justifyContent: "center",
            alignItems: "center"
          }}
        >
          <Image
            source={require("../../assets/404.png")}
            style={{
              width: 400,
              height: 300,
              marginTop: 40
            }}
          />
        </View>
      </View>
    );
  }
}
