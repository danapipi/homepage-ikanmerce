import React, { Component } from "react";
import {
  View,
  Text,
  AsyncStorage,
  TextInput,
  TouchableOpacity
} from "react-native";
import CommonHeader from "../../assets/CommonHeader";
import Axios from "axios";

export default class Cart extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cart: [],
      details: [],
      price: [],
      token: ""
    };
  }

  static navigationOptions = {
    header: null,
    drawerLabel: "Cart"
  };

  searchChange() {
    return "sukses";
  }
  clearSearch() {
    return "hilang";
  }

  async componentDidMount() {
    this.state.token = await AsyncStorage.getItem("token");
    this.loadCart();
  }

  async loadCart() {
    let cart = JSON.parse(await AsyncStorage.getItem("cart"));
    const details = [];
    this.setState({ cart });

    if (cart !== null)
      if (cart.length !== 0)
        await cart.forEach(async (cartItem, index) => {
          details[index] = (await Axios(
            `https://ikapi.herokuapp.com/api/items/${cartItem.id}`
          )).data.data[0];
          this.setState({ details });
        });
  }

  detailsData(cartItem, index) {
    if (this.state.details[index]) {
      let chosenOne = this.state.details[index].item_details.filter(
        filtItem => {
          return cartItem.item_detail_id == filtItem.id;
        }
      )[0];
      return (
        <View style={{ flexDirection: "row" }}>
          <View flex={1}>
            <Text>{chosenOne.size ? chosenOne.size : "no-size"}</Text>
            <Text>
              {"Rp " +
                chosenOne.price
                  .toFixed(2)
                  .replace(".", ",")
                  .replace(/\d(?=(\d{3})+\,)/g, "$&.")}
            </Text>
            <Text>{"Discount : " + chosenOne.discount + "%"}</Text>
          </View>
          {this.quantityChanger(cartItem, index, chosenOne)}
        </View>
      );
    } else {
      return (
        <View style={{ flexDirection: "row" }}>
          <View flex={1}>
            <Text>{cartItem.size}</Text>
            <Text>Details not loaded</Text>
          </View>
          {this.quantityChangerOffline(cartItem, index)}
        </View>
      );
    }
  }

  quantityChanger(cartItem, index, chosenOne) {
    this.state.price[index] =
      (chosenOne.price *
        this.state.cart[index].quantity *
        (100.0 - parseFloat(chosenOne.discount))) /
      100;
    return (
      <View>
        <View
          flexDirection="row"
          style={{
            alignContent: "center",
            justifyContent: "center",
            height: 50
          }}
        >
          <TouchableOpacity
            style={{
              width: 30,
              height: 30,
              alignContent: "center",
              justifyContent: "center",
              borderRadius: 5,
              backgroundColor: "#45aaf2",
              margin: 10
            }}
            onPress={() => {
              this.state.cart[index].quantity =
                this.state.cart[index].quantity - 1;
              if (this.state.cart[index].quantity <= 0) {
                (async () => {
                  delete this.state.cart[index];
                  delete this.state.price[index];
                  delete this.state.details[index];

                  this.state.cart = await this.state.cart.filter(x => x);
                  this.state.details = await this.state.details.filter(x => x);
                  this.state.price = await this.state.price.filter(x => x);
                  this.forceUpdate();
                })();
              } else this.forceUpdate();
            }}
          >
            <Text
              style={{
                fontSize: 40,
                textAlign: "center",
                textAlignVertical: "center",
                color: "white",
                alignSelf: "center"
              }}
            >
              -
            </Text>
          </TouchableOpacity>
          <TextInput
            style={{
              fontSize: 20,
              textAlign: "center",
              textAlignVertical: "center"
            }}
            defaultValue={String(cartItem.quantity)}
            keyboardType="number-pad"
            onChangeText={text =>
              (this.state.cart[index].quantity = parseInt(text))
            }
          />
          <TouchableOpacity
            style={{
              width: 30,
              height: 30,
              alignContent: "center",
              justifyContent: "center",
              borderRadius: 5,
              backgroundColor: "#45aaf2",
              margin: 10
            }}
            onPress={() => {
              this.state.cart[index].quantity =
                this.state.cart[index].quantity + 1;
              this.forceUpdate();
            }}
          >
            <Text
              style={{
                fontSize: 30,
                textAlign: "center",
                textAlignVertical: "center",
                color: "white"
              }}
            >
              +
            </Text>
          </TouchableOpacity>
        </View>

        <Text style={{ alignSelf: "center", fontSize: 16 }}>
          {"Rp " +
            this.state.price[index]
              .toFixed(2)
              .replace(".", ",")
              .replace(/\d(?=(\d{3})+\,)/g, "$&.")}
        </Text>
      </View>
    );
  }

  quantityChangerOffline(cartItem, index) {
    return (
      <View>
        <View
          flexDirection="row"
          style={{
            alignContent: "center",
            justifyContent: "center",
            height: 50
          }}
        >
          <TouchableOpacity
            style={{
              width: 30,
              height: 30,
              alignContent: "center",
              justifyContent: "center",
              borderRadius: 5,
              backgroundColor: "#45aaf2",
              margin: 10
            }}
            onPress={() => {
              this.state.cart[index].quantity =
                this.state.cart[index].quantity - 1;

              this.forceUpdate();
            }}
          >
            <Text
              style={{
                fontSize: 40,
                textAlign: "center",
                textAlignVertical: "center",
                color: "white",
                alignSelf: "center"
              }}
            >
              -
            </Text>
          </TouchableOpacity>
          <TextInput
            style={{
              fontSize: 20,
              textAlign: "center",
              textAlignVertical: "center"
            }}
            defaultValue={String(cartItem.quantity)}
            keyboardType="number-pad"
            onChangeText={text =>
              (this.state.cart[index].quantity = parseInt(text))
            }
          />
          <TouchableOpacity
            style={{
              width: 30,
              height: 30,
              alignContent: "center",
              justifyContent: "center",
              borderRadius: 5,
              backgroundColor: "#45aaf2",
              margin: 10
            }}
            onPress={() => {
              this.state.cart[index].quantity =
                this.state.cart[index].quantity + 1;
              this.forceUpdate();
            }}
          >
            <Text
              style={{
                fontSize: 30,
                textAlign: "center",
                textAlignVertical: "center",
                color: "white"
              }}
            >
              +
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }

  cartView() {
    return (
      <View>
        {this.state.cart.map((cartItem, index) => {
          return (
            <View
              style={{
                flexDirection: "row",
                padding: 5,
                marginVertical: 5,
                backgroundColor: "white",
                elevation: 4
              }}
              key={index}
            >
              <View flex={1}>
                <Text style={{ fontSize: 20 }}>
                  {this.state.details[index]
                    ? this.state.details[index].item_name
                    : cartItem.name + " (Storage)"}
                </Text>
                {this.detailsData(cartItem, index)}
              </View>
            </View>
          );
        })}
        <TouchableOpacity
          style={{
            alignContent: "center",
            justifyContent: "center",
            borderRadius: 20,
            backgroundColor: "#45aaf2",
            margin: 5,
            padding: 5
          }}
          onPress={() => {
            Axios("https://ikapi.herokuapp.com/api/cart", {
              headers: {
                Authorization: "Bearer " + this.state.token
              },
              method: "POST",
              data: this.state.cart.map(itemA => {
                return {
                  item_id: itemA.id,
                  item_detail_id: itemA.item_detail_id,
                  quantity: itemA.quantity
                };
              })
            })
              .then(async res => {
                console.log(res);
                await AsyncStorage.removeItem("cart");
                this.state.cart = await [];
                this.state.price = await [];
                this.state.details = await [];
                this.forceUpdate();
                this.props.navigation.navigate("Checkout", {
                  cartnum: res.data.cart_number
                });
              })
              .catch(err => console.log(err));
          }}
        >
          <Text
            style={{
              fontSize: 18,
              textAlign: "center",
              textAlignVertical: "center",
              color: "white",
              fontWeight: "bold"
            }}
          >
            Checkout
            {this.state.price.length !== 0
              ? " : Rp " +
                this.state.price
                  .reduce((a, b) => a + b)
                  .toFixed(2)
                  .replace(".", ",")
                  .replace(/\d(?=(\d{3})+\,)/g, "$&.")
              : null}
          </Text>
        </TouchableOpacity>
      </View>
    );
  }

  componentWillUnmount() {
    AsyncStorage.setItem("cart", JSON.stringify(this.state.cart));
  }

  render() {
    const navigation = this.props.navigation;
    return (
      <View flex={1}>
        <CommonHeader parent={this} />
        {Array.isArray(this.state.cart) ? (
          this.state.cart.length > 0 ? (
            this.cartView()
          ) : (
            <Text
              style={{
                margin: 5,
                padding: 20,
                backgroundColor: "white",
                elevation: 4,
                fontSize: 20,
                fontWeight: "bold",
                textAlign: "center"
              }}
            >
              Nothing in cart
            </Text>
          )
        ) : (
          <Text
            style={{
              margin: 5,
              padding: 20,
              backgroundColor: "white",
              elevation: 4,
              fontSize: 20,
              fontWeight: "bold",
              textAlign: "center"
            }}
          >
            Nothing in cart
          </Text>
        )}
      </View>
    );
  }
}
