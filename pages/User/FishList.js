import React, { Component } from "react";
import {
  View,
  Text,
  Image,
  ScrollView,
  Dimensions,
  TouchableOpacity,
  ActivityIndicator,
  Alert
} from "react-native";
import {
  SearchBar,
  Header,
  Icon,
  List,
  ListItem,
  Avatar
} from "react-native-elements";
import Axios from "axios";
import CommonHeader from "../../assets/CommonHeader";

export default class FishList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      page: 1,
      listItem: []
    };
  }

  static navigationOptions = {
    header: null,
    drawerLabel: "Fish List"
  };

  searchChange() {
    return "sukses";
  }
  clearSearch() {
    return "hilang";
  }

  arrayMake(initArr) {
    return initArr.reduce(
      (rows, key, index) =>
        (index % 2 == 0 ? rows.push([key]) : rows[rows.length - 1].push(key)) &&
        rows,
      []
    );
  }

  componentDidMount() {
    this.loadListL(1);
  }

  componentWillUnmount() {
    this.props.navigation.setParams({ category: undefined });
    this.props.navigation.setParams({ search: undefined });
  }

  loadListL(page) {
    Axios(
      `https://ikapi.herokuapp.com/api/items/?limit=6&page=${String(page)}${
        this.props.navigation.getParam("category", undefined)
          ? "&cat=" + this.props.navigation.getParam("category", undefined)
          : ""
      }${
        this.props.navigation.getParam("tags", undefined)
          ? "&tag=" + this.props.navigation.getParam("tags", undefined)
          : ""
      }${
        this.props.navigation.getParam("search", undefined)
          ? "&search=" + this.props.navigation.getParam("search", undefined)
          : ""
      }`
    ).then(response => {
      this.setState({ listItem: response.data.data });
    });
  }

  async sendParam(searchy) {
    console.log("sending", searchy);
    this.state.listItem = await [];

    this.props.navigation.state.params = await {
      search: searchy
    };
    this.loadListL(1);
  }

  viewList() {
    console.log(this.state.listItem);
    const navigation = this.props.navigation;
    return this.arrayMake(this.state.listItem).map((item, index) => {
      console.warn(item[0]);
      return (
        <View key={item} style={{ flexDirection: "row" }}>
          <TouchableOpacity
            style={{
              paddingBottom: 5,
              flex: 1,
              width: Dimensions.get("window").width / 2 - 20,
              margin: 5,
              backgroundColor: "white",
              borderRadius: 5,
              elevation: 4,
              overflow: "hidden"
            }}
            onPress={() =>
              navigation.navigate("Details", {
                id: item[0].id,
                send: this.sendParam.bind(this)
              })
            }
          >
            <Image
              style={{
                width: Dimensions.get("window").width / 2 - 10,
                height: Dimensions.get("window").width / 2 - 10,
                overflow: "hidden"
              }}
              resizeMode="cover"
              source={{
                uri: "https://ikapi.herokuapp.com/" + item[0].featured_image
              }}
            />
            <Text
              style={{
                marginTop: 5,
                fontSize: 18,
                marginHorizontal: 5
              }}
            >
              {String(item[0].item_name)}
            </Text>
            <View
              style={{
                flexDirection: "row",
                overflow: "scroll",
                flexWrap: "nowrap",
                alignItems: "flex-start"
              }}
            >
              {item[0].tags.map((tag, index) => {
                return (
                  <Text
                    key={tag.id}
                    style={{
                      margin: 5,
                      padding: 5,
                      borderRadius: 15,
                      borderWidth: 0.5,
                      fontSize: 12,
                      borderColor: "lightgrey",
                      backgroundColor: "white",
                      elevation: 4
                    }}
                  >
                    {tag.tag_name}
                  </Text>
                );
              })}
            </View>
          </TouchableOpacity>
          {item[1] !== undefined ? (
            <TouchableOpacity
              style={{
                paddingBottom: 5,
                flex: 1,
                width: Dimensions.get("window").width / 2 - 20,
                margin: 5,
                backgroundColor: "white",
                borderRadius: 5,
                elevation: 4,
                overflow: "hidden"
              }}
              onPress={() =>
                navigation.navigate("Details", {
                  id: item[1].id,
                  send: this.sendParam.bind(this)
                })
              }
            >
              <Image
                style={{
                  width: Dimensions.get("window").width / 2 - 10,
                  height: Dimensions.get("window").width / 2 - 10,
                  overflow: "hidden"
                }}
                resizeMode="cover"
                source={{
                  uri: "https://ikapi.herokuapp.com/" + item[1].featured_image
                }}
              />
              <Text
                style={{
                  marginTop: 5,
                  fontSize: 18,
                  marginHorizontal: 5
                }}
              >
                {String(item[1].item_name)}
              </Text>
              <View
                style={{
                  flexDirection: "row",
                  overflow: "scroll",
                  flexWrap: "nowrap",
                  alignItems: "flex-start"
                }}
              >
                {item[1].tags.map((tag, index) => {
                  return (
                    <Text
                      key={tag.id}
                      style={{
                        margin: 5,
                        padding: 5,
                        borderRadius: 15,
                        borderWidth: 0.5,
                        fontSize: 12,
                        borderColor: "lightgrey",
                        backgroundColor: "white",
                        elevation: 4
                      }}
                    >
                      {tag.tag_name}
                    </Text>
                  );
                })}
              </View>
            </TouchableOpacity>
          ) : (
            <View style={{ flex: 1, margin: 5 }} />
          )}
        </View>
      );
    });
  }

  viewButton() {
    return (
      <View style={{ flexDirection: "row" }}>
        <TouchableOpacity
          style={{
            margin: 10,
            padding: 5,
            elevation: 4,
            borderRadius: 20,
            backgroundColor: "#45aaf2",
            alignContent: "center",
            justifyContent: "center",
            flex: 1
          }}
          onPress={() => {
            if (this.state.page >= 2) this.state.page--;
            this.loadListL(this.state.page);
          }}
        >
          <Text
            style={{
              fontSize: 18,
              color: "white",
              fontWeight: "bold",
              textAlign: "center"
            }}
          >
            {"<< Prev Page"}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={{
            margin: 10,
            padding: 5,
            elevation: 4,
            borderRadius: 20,
            backgroundColor: "#45aaf2",
            alignContent: "center",
            justifyContent: "center",
            flex: 1
          }}
          onPress={() => {
            this.state.page++;
            this.loadListL(this.state.page);
          }}
        >
          <Text
            style={{
              fontSize: 18,
              color: "white",
              fontWeight: "bold",
              textAlign: "center"
            }}
          >
            Next Page >>
          </Text>
        </TouchableOpacity>
      </View>
    );
  }

  waity() {
    if (this.state.page !== 1) {
      this.state.page--;
      this.loadListL(this.state.page);
      Alert.alert("Last Page Reached");
      return <View />;
    } else {
      return (
        <View
          style={{
            height: Dimensions.get("window").height - 95,
            alignContent: "center",
            justifyContent: "center"
          }}
        >
          <ActivityIndicator size="large" />
          <Text
            style={{
              margin: 5,
              padding: 20,
              fontSize: 20,
              fontWeight: "bold",
              textAlign: "center"
            }}
          >
            No Item
          </Text>
        </View>
      );
    }
  }

  render() {
    return (
      <View>
        <CommonHeader parent={this} />

        {this.state.listItem.length == 0 ? (
          this.waity()
        ) : (
          <ScrollView
            style={{
              height: Dimensions.get("window").height - 95
            }}
          >
            {this.viewList()}
            {this.viewButton()}
          </ScrollView>
        )}
      </View>
    );
  }
}
