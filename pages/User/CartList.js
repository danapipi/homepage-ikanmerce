import React, { Component } from "react";
import {
  View,
  Text,
  AsyncStorage,
  TouchableOpacity,
  ScrollView,
  Dimensions,
  Alert
} from "react-native";
import CommonHeader from "../../assets/CommonHeader";
import Axios from "axios";

export default class Cart extends Component {
  constructor(props) {
    super(props);
    this.state = {
      carts: [],
      prices: [],
      token: ""
    };
  }

  static navigationOptions = {
    header: null,
    drawerLabel: "Cart List"
  };

  async componentDidMount() {
    this.state.token = await AsyncStorage.getItem("token");
    this.loadCart();
  }

  async loadCart() {
    Axios(`https://ikapi.herokuapp.com/api/cart`, {
      headers: {
        Authorization: "Bearer " + this.state.token
      }
    }).then(res => {
      console.log(res);
      this.setState({ carts: res.data.viewCart }, () => this.forceUpdate());
    });
  }

  render() {
    const navigation = this.props.navigation;
    let cartsView = [];
    if (this.state.carts.length > 0) {
      const cartNum = this.state.carts
        .map(itemCart => itemCart.cart_number)
        .filter((a, index, c) => c.indexOf(a) === index);

      cartsView = cartNum
        .map(numNum =>
          this.state.carts.filter(cartI => cartI.cart_number === numNum)
        )
        .sort((dimDim, dumDum) => {
          return dumDum[0].cart_number - dimDim[0].cart_number;
        });
      console.log(cartsView);
    }

    return (
      <View>
        <CommonHeader parent={this} />
        <View style={{ height: Dimensions.get("window").height - 95 }}>
          <ScrollView>
            {cartsView.length > 0 ? (
              cartsView.map((ins, indexEx) => {
                this.state.prices[indexEx] = [];
                return (
                  <TouchableOpacity
                    style={{
                      padding: 5,
                      marginVertical: 5,
                      backgroundColor: "white",
                      elevation: 4
                    }}
                    onPress={() =>
                      !ins[0].status
                        ? navigation.navigate("Checkout", {
                            cartnum: ins[0].cart_number
                          })
                        : Alert.alert(
                            "Invoice has been issued",
                            "Please check your email for your invoice."
                          )
                    }
                  >
                    <View>
                      <Text
                        style={{ fontSize: 20, fontWeight: "bold", margin: 5 }}
                      >
                        Cart {ins[0].cart_number}
                      </Text>
                      <Text
                        style={{ fontSize: 20, fontWeight: "bold", margin: 5 }}
                      >
                        {ins[0].status
                          ? "Invoice Made"
                          : "Invoice has not been made"}
                      </Text>
                    </View>
                    <View style={{ margin: 10 }}>
                      {ins.map((mini, indexDeep) => {
                        this.state.prices[indexEx][indexDeep] =
                          (mini.item_details[0].price *
                            mini.quantity *
                            (100.0 -
                              parseFloat(mini.item_details[0].discount))) /
                          100;

                        return (
                          <View>
                            <View flexDirection="row">
                              <Text
                                style={{
                                  fontSize: 18,
                                  fontWeight: "bold",
                                  flex: 1
                                }}
                              >
                                {mini.items[0].item_name}
                                <Text
                                  style={{ fontSize: 16, fontWeight: "bold" }}
                                >
                                  {" "}
                                  {mini.item_details[0].size}
                                </Text>
                                <Text
                                  style={{ fontSize: 12, fontWeight: "normal" }}
                                >
                                  x{mini.quantity}
                                </Text>
                              </Text>
                              <Text>
                                {"Rp " +
                                  this.state.prices[indexEx][indexDeep]
                                    .toFixed(2)
                                    .replace(".", ",")
                                    .replace(/\d(?=(\d{3})+\,)/g, "$&.")}
                              </Text>
                            </View>
                            <Text>{mini.items[0].description}</Text>
                          </View>
                        );
                      })}
                    </View>
                    <Text
                      style={{
                        fontSize: 18,
                        fontWeight: "bold",
                        margin: 5,
                        textAlign: "right"
                      }}
                    >
                      Total : Rp{" "}
                      {this.state.prices[indexEx]
                        .reduce((a, b) => {
                          return a + b;
                        }, 0)
                        .toFixed(2)
                        .replace(".", ",")
                        .replace(/\d(?=(\d{3})+\,)/g, "$&.")}
                    </Text>
                  </TouchableOpacity>
                );
              })
            ) : (
              <Text
                style={{
                  margin: 5,
                  padding: 20,
                  backgroundColor: "white",
                  elevation: 4,
                  fontSize: 20,
                  fontWeight: "bold",
                  textAlign: "center"
                }}
              >
                {" "}
                No Carts
              </Text>
            )}
          </ScrollView>
        </View>
      </View>
    );
  }
}
