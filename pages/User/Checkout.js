import React, { Component } from "react";
import {
  View,
  Text,
  AsyncStorage,
  TextInput,
  TouchableOpacity,Linking
} from "react-native";
import CommonHeader from "../../assets/CommonHeader";
import Axios from "axios";

export default class Cart extends Component {
  constructor(props) {
    super(props);
    this.state = {
      token: "",
      cartNum: "",
      recipient_name: "",
      phone: "",
      address: "",
      city: "",
      province: ""
    };
  }

  static navigationOptions = {
    header: null,
    drawerLabel: () => null
  };

  async componentDidMount() {
    this.state.token = await AsyncStorage.getItem("token");
    this.setState({
      cartNum: this.props.navigation.getParam("cartnum", "None")
    });
  }

  pay() {
    let {
      recipient_name,
      phone,
      address,
      city,
      province,
      cartNum
    } = this.state;
    const data = { recipient_name, phone, address, city, province, cartNum };

    console.log(data);
    Axios("https://ikapi.herokuapp.com/api/invoice/", {
      headers: {
        Authorization: "Bearer " + this.state.token
      },
      method: "POST",
      data: data
    })
      .then(res => {
        this.props.navigation.navigate("Cart History");
        Linking.openURL(res.data.link_payment.redirect_url);
      })
      .catch(err => console.log("Err",err));
  }

  render() {
    const navigation = this.props.navigation;
    return (
      <View flex={1}>
        <CommonHeader parent={this} />
        <View
          style={{
            margin: 10,
            padding: 10,
            backgroundColor: "white",
            elevation: 4,
            borderRadius: 5
          }}
        >
          <Text
            style={{ textAlign: "center", fontWeight: "bold", fontSize: 18 }}
          >
            Your Cart Number Is {this.state.cartNum}
          </Text>

          <View>
            <TextInput
              style={{
                backgroundColor: "white",
                borderColor: "lightgrey",
                borderRadius: 3,
                marginTop: 8
              }}
              onSubmitEditing={() => {
                this.phoneInput.focus();
              }}
              blurOnSubmit={false}
              placeholder="Nama Penerima"
              returnKeyType="next"
              onChangeText={recipient_name => this.setState({ recipient_name })}
            />

            <TextInput
              style={{
                backgroundColor: "white",
                borderColor: "lightgrey",
                borderRadius: 3,
                marginTop: 8
              }}
              ref={input => {
                this.phoneInput = input;
              }}
              onSubmitEditing={() => {
                this.alamatInput.focus();
              }}
              blurOnSubmit={false}
              placeholder="Phone Number"
              returnKeyType="next"
              onChangeText={phone => this.setState({ phone })}
              keyboardType="phone-pad"
            />

            <TextInput
              style={{
                backgroundColor: "white",
                borderColor: "lightgrey",
                borderRadius: 3,
                marginTop: 8
              }}
              ref={input => {
                this.alamatInput = input;
              }}
              onSubmitEditing={() => this.kotaInput.focus()}
              placeholder="Alamat Pengiriman"
              returnKeyType="next"
              onChangeText={address => this.setState({ address })}
            />

            <TextInput
              style={{
                backgroundColor: "white",
                borderColor: "lightgrey",
                borderRadius: 3,
                marginTop: 8
              }}
              ref={input => {
                this.kotaInput = input;
              }}
              onSubmitEditing={() => this.provInput.focus()}
              placeholder="Kota"
              returnKeyType="next"
              onChangeText={city => this.setState({ city })}
            />

            <TextInput
              style={{
                backgroundColor: "white",
                borderColor: "lightgrey",
                borderRadius: 3,
                marginTop: 8
              }}
              ref={input => {
                this.provInput = input;
              }}
              placeholder="Provinsi"
              onChangeText={province => this.setState({ province })}
              onSubmitEditing={this.pay.bind(this)}
            />
          </View>

          <TouchableOpacity
            style={{
              margin: 10,
              padding: 10,
              backgroundColor: "#45aaf2",
              elevation: 4,
              borderRadius: 20
            }}
            onPress={this.pay.bind(this)}
          >
            <Text
              style={{
                color: "white",
                textAlign: "center",
                fontWeight: "bold",
                fontSize: 18
              }}
            >
              Proceed to payment
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}
