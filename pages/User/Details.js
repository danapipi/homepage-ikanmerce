import React, { Component } from "react";
import {
  View,
  TouchableOpacity,
  ImageBackground,
  Dimensions,
  ScrollView,
  ActivityIndicator,
  AsyncStorage,
  Alert
} from "react-native";
import { Text } from "react-native-elements";
import Axios from "axios";
import CommonHeader from "../../assets/CommonHeader";

export default class Details extends Component {
  constructor(props) {
    super(props);
    this.state = {
      xOffset: 0,
      item: {},
      update: 0,
      isDetail:true
    };
  }

  static navigationOptions = {
    header: null
  };

  componentDidMount() {
    this.state.intervalSlide = setInterval(() => {
      this.scrolly.scrollTo({
        x:
          this.state.xOffset + Dimensions.get("window").width <=
          (this.state.item.item_images.length - 0.1) *
            Dimensions.get("window").width
            ? this.state.xOffset + Dimensions.get("window").width
            : 0
      });
    }, 10000);
    Axios(
      `https://ikapi.herokuapp.com/api/items/${this.props.navigation.getParam(
        "id",
        0
      )}`
    ).then(response => {
      this.setState({ item: response.data.data[0] });
    });
  }

  componentWillUnmount() {
    clearInterval(this.state.intervalSlide);
  }

  variationView() {
    return this.state.item.item_details.map((item, index) => {
      return (
        <View
          key={index}
          style={{
            borderBottomWidth: 1,
            marginHorizontal: 20,
            padding: 5,
            flexDirection: "row",
            borderColor: "lightgrey"
          }}
        >
          <View style={{ flex: 1 }}>
            <Text>{"Size : " + item.size}</Text>
            <Text>
              {"Price : Rp" +
                item.price
                  .toFixed(2)
                  .replace(".", ",")
                  .replace(/\d(?=(\d{3})+\,)/g, "$&.")}
            </Text>
            <Text>{"Stock : " + String(item.stock)}</Text>
            <Text>{"Discount : " + item.discount}</Text>
          </View>

          <TouchableOpacity
            style={{
              height: 40,
              padding: 5,
              borderRadius: 10,
              borderWidth: 1,
              justifyContent: "center",
              borderColor: "#45aaf2"
            }}
            onPress={() => {
              this.addToCart(item, index);
            }}
          >
            <Text>Add to Cart</Text>
          </TouchableOpacity>
        </View>
      );
    });
  }

  async addToCart(itemDetails, index) {
    const data = {
      id: itemDetails.item_id,
      item_detail_id: itemDetails.id,
      quantity: 1,
      size: itemDetails.size,
      name: this.state.item.item_name
    };
    currCart = await AsyncStorage.getItem("cart");
    console.log("Current Cart : ", currCart);

    if (
      !currCart ||
      currCart === null ||
      currCart === "null" ||
      currCart.length === 0
    ) {
      try {
        await AsyncStorage.setItem("cart", JSON.stringify([data]));
        Alert.alert("Added to Cart");
      } catch (err) {
        console.log("Null Err : ", err);
      }
    } else {
      try {
        let bool = [...JSON.parse(currCart)]
          .map(
            itemDetails =>
              itemDetails.id === data.id &&
              itemDetails.item_detail_id === data.item_detail_id
          )
          .reduce((a, b) => a || b, false);
        if (!bool) {
          await AsyncStorage.setItem(
            "cart",
            JSON.stringify([...JSON.parse(currCart), data])
          );
          Alert.alert("Added to Cart");
        } else Alert.alert("Item is in Cart");
      } catch (err) {
        console.log("NonNull Err : ", err);
      }
    }

    this.setState({ update: this.state.update + 1 });
  }

  render() {
    return (
      <View>
        <CommonHeader parent={this} back />
        {Object.keys(this.state.item).length === 0 ? (
          <View
            style={{
              height: Dimensions.get("window").height - 95,
              alignContent: "center",
              justifyContent: "center"
            }}
          >
            <ActivityIndicator size="large" />
          </View>
        ) : (
          <ScrollView
            style={{
              height: Dimensions.get("window").height - 95,
              width: Dimensions.get("window").width
            }}
          >
            <ScrollView
              style={{ height: 210, borderWidth: 0.5 }}
              horizontal={true}
              pagingEnabled={true}
              ref={ref => (this.scrolly = ref)}
              onScroll={event => {
                this.state.xOffset = event.nativeEvent.contentOffset.x;
              }}
              onScrollEndDrag={event => {
                this.state.xOffset = event.nativeEvent.contentOffset.x;
              }}
              onScrollAnimationEnd={event => {
                this.state.xOffset = event.nativeEvent.contentOffset.x;
              }}
            >
              {this.state.item.item_images.map((itemImage, index) => {
                return (
                  <ImageBackground
                    key={index}
                    style={{
                      width: Dimensions.get("window").width,
                      justifyContent: "flex-end",
                      alignItems: "center"
                    }}
                    source={{
                      uri: "https://ikapi.herokuapp.com/" + itemImage.image
                    }}
                    resizeMode="cover"
                  >
                    <Text style={{ color: "white" }}>
                      {itemImage.image_name}
                    </Text>
                  </ImageBackground>
                );
              })}
            </ScrollView>

            <View
              style={{
                padding: 6,
                backgroundColor: "white",
                elevation: 3,
                fontWeight: "bold"
              }}
            >
              <Text style={{ fontSize: 22 }}>{this.state.item.item_name}</Text>
              <Text style={{ fontSize: 12 }}>
                {this.state.item.category.category_name}
              </Text>
            </View>

            <View
              style={{
                marginTop: 10,
                backgroundColor: "white",
                elevation: 2,
                padding: 5
              }}
            >
              <Text
                style={{ fontSize: 11, marginVertical: 10, fontWeight: "bold" }}
              >
                Deskripsi Produk
              </Text>
              <Text style={{ fontSize: 14, marginHorizontal: 5 }}>
                {this.state.item.description}
              </Text>
              <Text
                style={{ fontSize: 11, marginVertical: 10, fontWeight: "bold" }}
              >
                Spesifikasi
              </Text>
              <Text style={{ fontSize: 14, marginHorizontal: 5 }}>
                {this.state.item.specification}
              </Text>
            </View>

            <View
              style={{
                marginTop: 10,
                backgroundColor: "white",
                elevation: 2,
                padding: 5
              }}
            >
              <Text
                style={{ fontSize: 11, marginVertical: 10, fontWeight: "bold" }}
              >
                Tag
              </Text>

              <View
                style={{
                  flexDirection: "row",
                  flexWrap: "wrap",
                  alignItems: "flex-start"
                }}
              >
                {this.state.item.tags.map((tag, index) => {
                  return (
                    <Text
                      key={tag.id}
                      style={{
                        margin: 5,
                        padding: 5,
                        borderRadius: 15,
                        borderWidth: 0.5,
                        fontSize: 12,
                        borderColor: "lightgrey",
                        backgroundColor: "white",
                        elevation: 4
                      }}
                    >
                      {tag.tag_name}
                    </Text>
                  );
                })}
              </View>
            </View>

            <View
              style={{
                marginTop: 10,
                backgroundColor: "white",
                elevation: 4,
                padding: 5
              }}
            >
              <Text
                style={{
                  fontSize: 11,
                  marginVertical: 10,
                  fontWeight: "bold"
                }}
              >
                Variations
              </Text>
              {this.variationView()}
            </View>
          </ScrollView>
        )}
      </View>
    );
  }
}

