import React, { Component } from "react";
import Axios from "axios";
import {
  Button,
  View,
  TextInput,
  Alert,
  Picker
} from "react-native";

export default class RegisterScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      email: "",
      phone: "",
      address: "",
      gender: "pria",
      password: "",
      password2: ""
    };
  }

  static navigationOptions = {
    header: null
  };

  render() {
    return (
      <View style={{ flex: 1 }}>
        <View style={{ flex: 1 }} />
        <View style={{ margin: 30 }}>
          <TextInput
            style={{
              backgroundColor: "white",
              borderColor: "lightgrey",
              borderRadius: 3,
              marginTop: 8
            }}
            onSubmitEditing={() => {
              this.emailInput.focus();
            }}
            blurOnSubmit={false}
            placeholder="Name"
            onChangeText={name => this.setState({ name })}
          />

          <TextInput
            style={{
              backgroundColor: "white",
              borderColor: "lightgrey",
              borderRadius: 3,
              marginTop: 8
            }}
            ref={input => {
              this.emailInput = input;
            }}
            onSubmitEditing={() => this.phoneInput.focus()}
            placeholder="Email"
            onChangeText={email => this.setState({ email })}
            keyboardType="email-address"
          />

          <TextInput
            style={{
              backgroundColor: "white",
              borderColor: "lightgrey",
              borderRadius: 3,
              marginTop: 8
            }}
            ref={input => {
              this.phoneInput = input;
            }}
            onSubmitEditing={() => {
              this.addressInput.focus();
            }}
            blurOnSubmit={false}
            placeholder="Phone Number"
            onChangeText={phone => this.setState({ phone })}
            keyboardType="phone-pad"
          />

          <TextInput
            style={{
              backgroundColor: "white",
              borderColor: "lightgrey",
              borderRadius: 3,
              marginTop: 8
            }}
            ref={input => {
              this.addressInput = input;
            }}
            onSubmitEditing={() => {
              this.passInput.focus();
            }}
            blurOnSubmit={false}
            placeholder="Address"
            onChangeText={address => this.setState({ address })}
            keyboardType="phone-pad"
          />

          <Picker
            selectedValue={this.state.gender}
            style={{ height: 50, width: 200 }}
            onValueChange={(gender, pickGenderInd) => {
              this.setState({ gender, pickGenderInd });
              this.pickerChange(pickGenderInd);
            }}
          >
            <Picker.Item label="Male" value="pria" />
            <Picker.Item label="Female" value="wanita" />
          </Picker>

          <TextInput
            style={{
              backgroundColor: "white",
              borderColor: "lightgrey",
              borderRadius: 3,
              marginTop: 8
            }}
            secureTextEntry={true}
            ref={input => {
              this.passInput = input;
            }}
            onSubmitEditing={() => {
              this.pass2Input.focus();
            }}
            blurOnSubmit={false}
            placeholder="Password"
            onChangeText={password => this.setState({ password })}
          />

          <TextInput
            style={{
              backgroundColor: "white",
              borderColor: "lightgrey",
              borderRadius: 3,
              marginTop: 8
            }}
            secureTextEntry={true}
            ref={input => {
              this.pass2Input = input;
            }}
            onSubmitEditing={this.onPressRegis.bind(this)}
            blurOnSubmit={false}
            placeholder="Verify Password"
            onChangeText={password2 => this.setState({ password2 })}
          />

          <View style={{ height: 20 }} />
          <Button title="Register" onPress={this.onPressRegis.bind(this)} />
        </View>
      </View>
    );
  }

  onPressRegis() {
    const self = this;
    const data = ({
      name,
      email,
      phone,
      gender,
      address,
      password,
      password2
    } = this.state);
    Axios("https://ikapi.herokuapp.com/api/users/register", {
      method: "POST",
      data: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(response => {
        console.log("REGISTERED");
        self.props.navigation.navigate("Login");
      })
      .catch(err => {
        Alert.alert("ERROR", err);
        console.log("ERROR", err);
      });
  }
}
