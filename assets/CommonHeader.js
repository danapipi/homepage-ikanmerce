import React, { Component } from "react";
import { Dimensions } from "react-native";
import { NavigationActions } from "react-navigation";
import { SearchBar, Header, Icon, Text } from "react-native-elements";

export default class CommonHeader extends Component {
  constructor(props) {
    super(props);
    this.state = { search: "" };
  }

  searchSubmit() {
    if (this.props.parent.loadListL) {
      this.props.parent.state.listItem = [];
      this.props.parent.props.navigation.state.params = {
        search: this.state.search
      };
      this.props.parent.loadListL(1);
    } else if (this.props.parent.state.isDetail) {
      this.props.parent.props.navigation.navigate("FishList");
      this.props.parent.props.navigation.state.params.send(this.state.search);
    } else {
      const navAction = NavigationActions.navigate({
        routeName: "Fish List",
        action: NavigationActions.navigate({
          routeName: "FishList",
          params: { search: this.state.search }
        })
      });
      this.props.parent.props.navigation.dispatch(navAction);
    }
  }

  render() {
    const parent = this.props.parent;
    return (
      <Header
        elevation={4}
        innerContainerStyles={{
          flex: 1,
          flexDirection: "row",
          justifyContent: "space-between",
          alignItems: "center"
        }}
        leftComponent={
          <Icon
            name={this.props.back ? "arrow-back" : "menu"}
            color="#fff"
            onPress={() =>
              this.props.back
                ? parent.props.navigation.goBack()
                : parent.props.navigation.openDrawer()
            }
          />
        }
        centerComponent={
          this.props.admin ? null : (
            <SearchBar
              round
              lightTheme
              containerStyle={{
                width: Dimensions.get("window").width - 100,
                backgroundColor: "transparent",
                borderTopWidth: 0,
                borderBottomWidth: 0
              }}
              textInputRef={input => (this.SearchInput = input)}
              inputStyle={{ backgroundColor: "white", fontSize: 16 }}
              onChangeText={text => (this.state.search = text)}
              onClearText={() => this.SearchInput.clear()}
              onSubmitEditing={this.searchSubmit.bind(this)}
              icon={{ type: "font-awesome", name: "search" }}
              placeholder="Search fish here..."
            />
          )
        }
        rightComponent={
          <Icon
            name="fish"
            color="#fff"
            type="material-community"
            size={30}
            onPress={() => {
              parent.props.navigation.navigate("Cart");
            }}
          />
        }
        backgroundColor="#45aaf2"
      />
    );
  }
}
